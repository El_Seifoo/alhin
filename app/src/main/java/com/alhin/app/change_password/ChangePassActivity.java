package com.alhin.app.change_password;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.databinding.ActivityChangePassBinding;
import com.alhin.app.main.MainActivity;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class ChangePassActivity extends AppCompatActivity implements ChangePassViewModel.ViewListener, Presenter {
    private ChangePassViewModel viewModel;
    private ActivityChangePassBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_pass);
        viewModel = ViewModelProviders.of(this).get(ChangePassViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEditTextError(EditText editText, String error) {
        editText.setError(error);
    }

    @Override
    public void passwordChangedSuccessfully() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onSaveClicked() {
        viewModel.requestChangePassword(dataBinding.passwordEditText,
                dataBinding.confirmPasswordEditText,
                getIntent().getExtras().getString("tempToken"));
    }
}
