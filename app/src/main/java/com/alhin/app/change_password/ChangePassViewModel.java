package com.alhin.app.change_password;

import android.app.Application;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ChangePassViewModel extends AndroidViewModel implements ChangePassModel.ModelCallback {
    private ViewListener viewListener;
    private ChangePassModel model;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonClickable;

    public ChangePassViewModel(@NonNull Application application) {
        super(application);
        model = new ChangePassModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonClickable = new ObservableField<>(true);
    }

    protected void requestChangePassword(EditText passwordEditText, EditText confirmPasswordEditText, String tempToken) {
        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.password_length_error));
            return;
        }

        String confirmPassword = confirmPasswordEditText.getText().toString().trim();
        if (confirmPassword.isEmpty()) {
            viewListener.showEditTextError(confirmPasswordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (!password.equals(confirmPassword)) {
            viewListener.showEditTextError(confirmPasswordEditText, getApplication().getString(R.string.password_confirm_not_matching));
            return;
        }

        setProgress(View.VISIBLE);
        setButtonClickable(false);
        Map<String, String> map = new HashMap<>();
        map.put("tmpToken", tempToken);
        map.put("newPassword", password);
        model.changePass(map, this);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.something_went_wrong));
    }

    @Override
    public void handleResponse(ApiResponse<Void> response) {
        if (response == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            return;
        }

        if (response.getMessage().trim().toLowerCase().equals(getApplication().getString(R.string.done))) {
            viewListener.showToastMessage(getApplication().getString(R.string.password_changed_successfully));
            viewListener.passwordChangedSuccessfully();
            return;
        }

        viewListener.showToastMessage(response.getMessage());
    }


    // Setters & Getters ------------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Boolean> getButtonClickable() {
        return buttonClickable;
    }

    @Override
    public void setButtonClickable(boolean clickable) {
        this.buttonClickable.set(clickable);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }
    // Setters & Getters -----------> end


    protected interface ViewListener {

        void showToastMessage(String message);

        void showEditTextError(EditText editText, String error);

        void passwordChangedSuccessfully();
    }
}
