package com.alhin.app.otp;

public interface Presenter {
    void onResendClicked();
}
