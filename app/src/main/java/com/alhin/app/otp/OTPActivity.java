package com.alhin.app.otp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.change_password.ChangePassActivity;
import com.alhin.app.databinding.ActivityOtpBinding;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class OTPActivity extends AppCompatActivity implements OTPViewModel.ViewListener, Presenter {
    private OTPViewModel viewModel;
    private ActivityOtpBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_otp);
        viewModel = ViewModelProviders.of(this).get(OTPViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public String getEmail() {
        return getIntent().getExtras().getString("email");
    }

    @Override
    public void navigateChangePassword(String tempToken) {
        Intent intent = new Intent(this, ChangePassActivity.class);
        intent.putExtra("tempToken",tempToken);
        startActivity(intent);
    }

    @Override
    public void onResendClicked() {
        viewModel.requestResendCode(getEmail());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
