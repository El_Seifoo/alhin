package com.alhin.app.otp;

import android.app.Application;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class OTPViewModel extends AndroidViewModel implements OTPModel.ModelCallback {
    private ViewListener viewListener;
    private OTPModel model;
    private ObservableField<String> otp;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonClickable;

    public OTPViewModel(@NonNull Application application) {
        super(application);
        model = new OTPModel(application);
        otp = new ObservableField<>("");
        progress = new ObservableField<>(View.GONE);
        buttonClickable = new ObservableField<>(true);
    }


    protected void requestCheckOTP(String email, String otp) {
        Log.e("otp", otp);
        setProgress(View.VISIBLE);
        setButtonClickable(false);
        Map<String, String> map = new HashMap<>();
        map.put("email", email);
        map.put("code", otp);
        model.checkOTP(map, this);
    }

    protected void requestResendCode(String email) {
        setProgress(View.VISIBLE);
        setButtonClickable(false);
        Map<String, String> map = new HashMap<>();
        map.put("email", email);
        model.resendCode(map, this);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.something_went_wrong));
    }

    @Override
    public void handleResponse(ApiResponse<String> response) {
        if (response == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            return;
        }

        String message = response.getMessage().trim();
        if (message.equals(getApplication().getString(R.string.done))) {
            viewListener.showToastMessage(getApplication().getString(R.string.code_sent_successfully));
            viewListener.navigateChangePassword(response.getData());
            return;
        }

        viewListener.showToastMessage(message);
    }

    @Override
    public void handleResendCodeResponse(ApiResponse<Void> response) {
        if (response == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            return;
        }

        String message = response.getMessage().trim();
        if (message.equals(getApplication().getString(R.string.done))) {
            viewListener.showToastMessage(getApplication().getString(R.string.code_sent_successfully));

            return;
        }

        viewListener.showToastMessage(message.equals(getApplication().getString(R.string.login_response_email_not_found)) ?
                getApplication().getString(R.string.email_not_found) :
                getApplication().getString(R.string.something_went_wrong));
    }

    // Setters & Getters -----------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getOtp() {
        if (otp.get().length() == 6) {
            this.requestCheckOTP(viewListener.getEmail(), otp.get());
        }
        return otp;
    }

    public ObservableField<Boolean> getButtonClickable() {
        return buttonClickable;
    }

    public void setOtp(String otp) {
        this.otp.set(otp);
    }

    @Override
    public void setButtonClickable(boolean clickable) {
        this.buttonClickable.set(clickable);
    }


    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }
    // Setter & getter -----------> end

    protected interface ViewListener {

        void showToastMessage(String message);

        String getEmail();

        void navigateChangePassword(String tempToken);
    }
}
