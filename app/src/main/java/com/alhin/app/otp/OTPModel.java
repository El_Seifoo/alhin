package com.alhin.app.otp;

import android.content.Context;
import android.view.View;

import com.alhin.app.classes.ApiResponse;
import com.alhin.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPModel {
    private Context context;

    public OTPModel(Context context) {
        this.context = context;
    }

    protected void checkOTP(Map<String, String> map, final ModelCallback callback) {
        Call<ApiResponse<String>> call = MySingleton.getInstance(context).createService().checkOTP(map);
        call.enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                if (response.code() == 500) {
                    callback.handleResponse(null);
                    return;
                }
                if (response.code() == 200)
                    callback.handleResponse(response.body());
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }


    protected void resendCode(Map<String, String> map, final ModelCallback callback) {
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().sendCode(map);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                if (response.code() == 500) {
                    callback.handleResponse(null);
                    return;
                }
                if (response.code() == 200)
                    callback.handleResendCodeResponse(response.body());
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {

        void setProgress(int progress);

        void setButtonClickable(boolean clickable);

        void handleResponse(ApiResponse<String> String);

        void handleResendCodeResponse(ApiResponse<Void> String);

        void onFailureHandler(Throwable t);
    }
}
