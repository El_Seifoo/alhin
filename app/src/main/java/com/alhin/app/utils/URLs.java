package com.alhin.app.utils;

public class URLs {
    public static final String BASE = "http://apialhin.77carsael.com/";
    public static final String ROOT = BASE + "api/";
    public static final String REGISTER = "register/";
    public static final String LOGIN = "login";
    public static final String JOBS = "job/search";
    public static final String ADD_JOB = "job/add";
    public static final String APPLIED_JOBS = "appley/get";
    public static final String JOB_INFO = "job/info";
    public static final String APPLY = "appley";
    public static final String WITHDRAW = "unappley";
    public static final String CVS = "cv/search";
    public static final String CV_INFO = "cv/info";
    public static final String GET_CV = "cv/get";
    public static final String CREATE_CV = "cv/add";
    public static final String UPDATE_CV = "cv/update";
    public static final String COUNTRIES = "residenceCountry";
    public static final String JOBS_TITLES = "jobs/name";
    public static final String NATIONALITIES = "nationality";
    public static final String RELIGIONS = "religion";
    public static final String FAV_UN_FAV = "makeFavourite";
    public static final String FAVOURITES = "favourite";
    public static final String APP_INFO = "appInfo";
    public static final String CONTACT_US = "contact";
    public static final String SEND_CODE = "forgetPassword";
    public static final String CHECK_OTP = "validateCode";
    public static final String CHANGE_PASSWORD = "changePassword";
    public static final String CHANGE_LANGUAGE = "changeLang";
    public static final String AD = "ads";
}
