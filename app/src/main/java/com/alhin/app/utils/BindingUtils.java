package com.alhin.app.utils;

import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.alhin.app.classes.Country;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.classes.JobInfo;

import java.util.ArrayList;
import java.util.List;

public class BindingUtils {

    // check if string input data is cv or job (jobtitle , image)
    public static String decideCvOrJob(String cvData, String jobData) {
        return cvData == null ? jobData : cvData;
    }

    // check if input data is cv or job
    // index = 0 -> rate , index = 1 -> salary , index = 2 -> reviews
    public static String decideCvOrJob(CvInfo cv, JobInfo job, int index) {
        if (cv != null) {
            if (index == 0) return convertFloatToString(cv.getRate());
            if (index == 1) return convertDoubleToString(cv.getExpectedSalary());
            return convertIntToString(cv.getReviews());
        }

        if (index == 0) return convertFloatToString(job.getRate());
        if (index == 1) return convertDoubleToString(job.getSalary());
        return convertIntToString(job.getReviews());

    }

    // check if input data is cv or job
    public static boolean decideCvOrJob(CvInfo cv, JobInfo job) {
        return cv != null ? cv.isFavourite() : job.isFavourite();
    }

    // check if number list *.00 remove .00
    public static String convertFloatToString(float number) {
        String pointString = String.valueOf(number);
        return (number % 1 == 0 ? pointString.substring(0, pointString.indexOf(".")) : pointString);
    }

    // check if number list *.00 remove .00
    public static String convertDoubleToString(double number) {
        String pointString = String.valueOf(number);
        return (number % 1 == 0 ? pointString.substring(0, pointString.indexOf(".")) : pointString);
    }

    public static String convertIntToString(int number) {
        return number + "";
    }

    public static String setReviewsString(int number, String data) {
        return number + " " + data;
    }

    public static int convertVisibility(int visibility) {
        return visibility == View.VISIBLE ? View.GONE : View.VISIBLE;
    }
}
