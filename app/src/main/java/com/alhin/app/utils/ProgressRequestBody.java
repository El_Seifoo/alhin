package com.alhin.app.utils;

import android.os.Handler;
import android.os.Looper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

public class ProgressRequestBody extends RequestBody {
    private static final int DEFAULT_BUFFER_SIZE = 20000;

    private File file;
    private String path;
    private String contentType;
    private UploadCallback callback;


    public ProgressRequestBody(File file, String contentType, UploadCallback callback) {
        this.file = file;
        this.contentType = contentType;
        this.callback = callback;
    }



    @Nullable
    @Override
    public MediaType contentType() {
        return MediaType.parse(contentType + "/*");
    }

    @Override
    public long contentLength() throws IOException {
        return file.length();
    }

    @Override
    public void writeTo(@NotNull BufferedSink sink) throws IOException {
        long fileLength = file.length();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        FileInputStream in = new FileInputStream(file);
        long uploaded = 0;


        int read;
        Handler handler = new Handler(Looper.getMainLooper());
        while ((read = in.read(buffer)) != -1) {
            uploaded += read;
            sink.write(buffer, 0, read); // update progress on UI thread
            handler.post(new ProgressUpdater(uploaded, fileLength));
        }
        in.close();
    }


    private class ProgressUpdater implements Runnable {
        private Long uploaded;
        private long total;

        public ProgressUpdater(Long uploaded, long total) {
            this.uploaded = uploaded;
            this.total = total;
        }

        @Override
        public void run() {
            callback.onProgress((int) (100 * uploaded / total));
        }
    }

    public interface UploadCallback {
        void onProgress(int percentage);

        void onError();

        void onFinish();
    }
}
