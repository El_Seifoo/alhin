package com.alhin.app.utils;


import com.alhin.app.classes.Ad;
import com.alhin.app.classes.ApiListResponse;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.AppInfo;
import com.alhin.app.classes.AppliedJob;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.classes.CvObj;
import com.alhin.app.classes.FavouritesResponse;
import com.alhin.app.classes.JobCreationObj;
import com.alhin.app.classes.JobInfo;
import com.alhin.app.classes.JobObj;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.classes.Nationality;
import com.alhin.app.classes.Religion;
import com.alhin.app.classes.UserInfo;
import com.alhin.app.classes.UserObj;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

public interface ApiServicesClient {

    @POST(URLs.REGISTER + "{userType}")
    Call<ApiResponse<UserInfo>> signUp(@Body UserObj userObj, @Path("userType") String type);

    @POST(URLs.LOGIN)
    Call<ApiResponse<UserInfo>> login(@Body UserObj userObj);

    @POST(URLs.JOBS)
    Call<ApiListResponse<JobObj>> search(@Body Map<String, Object> query);

    @Multipart
    @POST(URLs. ADD_JOB)
    Call<ApiResponse<Void>> addJob(@PartMap Map<String, RequestBody> partMap,
                                   @Part MultipartBody.Part file);

    @POST(URLs.APPLIED_JOBS)
    Call<ApiListResponse<AppliedJob>> appliedJobs(@Body Map<String, String> map);

    @POST(URLs.JOB_INFO)
    Call<ApiResponse<JobInfo>> jobInfo(@Body Map<String, Object> map);

    @POST(URLs.APPLY)
    Call<ApiResponse<Void>> apply(@Body Map<String, String> map);

    @POST(URLs.WITHDRAW)
    Call<ApiResponse<Void>> withdraw(@Body Map<String, String> map);

    @POST(URLs.CVS)
    Call<ApiListResponse<CvObj>> searchCVs(@Body Map<String, Object> query);

    @POST(URLs.CV_INFO)
    Call<ApiResponse<CvInfo>> cvInfo(@Body Map<String, Object> map);

    @POST(URLs.GET_CV)
    Call<ApiResponse<CvInfo>> getCv(@Body Map<String, String> map);

    @POST(URLs.UPDATE_CV)
    Call<ApiResponse<Void>> updateCv(@Body Map<String, Object> map);

    @Multipart
    @POST(URLs.UPDATE_CV)
    Call<ApiResponse<Void>> updateCvWithPhoto(@PartMap Map<String, RequestBody> partMap,
                                              @Part MultipartBody.Part file);

    @Multipart
    @POST(URLs.CREATE_CV)
    Call<ApiResponse<Void>> createCv(@PartMap Map<String, RequestBody> partMap,
                                     @Part MultipartBody.Part file);

    @POST(URLs.COUNTRIES)
    Call<ApiListResponse<Country>> countries(@Body Map<String, String> body);

    @POST(URLs.JOBS_TITLES)
    Call<ApiListResponse<JobTitle>> jobsTitles(@Body Map<String, String> body);

    @POST(URLs.NATIONALITIES)
    Call<ApiListResponse<Nationality>> nationalities(@Body Map<String, String> body);

    @POST(URLs.RELIGIONS)
    Call<ApiListResponse<Religion>> religions(@Body Map<String, String> body);

    @POST(URLs.FAV_UN_FAV)
    Call<ApiResponse<Void>> favUnFav(@Body Map<String, String> body);

    @POST(URLs.FAVOURITES)
    Call<FavouritesResponse> favourites(@Body Map<String, String> body);

    @POST(URLs.APP_INFO)
    Call<ApiResponse<AppInfo>> appInfo(@Body Map<String, String> body);

    @POST(URLs.CONTACT_US)
    Call<ApiResponse<Void>> contactUs(@Body Map<String, String> body);

    @POST(URLs.SEND_CODE)
    Call<ApiResponse<Void>> sendCode(@Body Map<String, String> map);

    @POST(URLs.CHECK_OTP)
    Call<ApiResponse<String>> checkOTP(@Body Map<String, String> map);

    @POST(URLs.CHANGE_PASSWORD)
    Call<ApiResponse<Void>> changePass(@Body Map<String, String> map);

    @POST(URLs.CHANGE_LANGUAGE)
    Call<ApiResponse<Void>> changeLang(@Body Map<String, String> map);

    @POST(URLs.AD)
    Call<ApiResponse<Ad>> fetchAd();
}
