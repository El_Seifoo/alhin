package com.alhin.app.utils;

public class Constants {
    public static final String SHARED_PREF_NAME = "alhin";
    public static final String USER_TOKEN = "token";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String APP_LANGUAGE = "app_language";
    public static final String USER_DATA = "user_data";
    public static final String IS_EMPLOYEE = "isEmployee";
}
