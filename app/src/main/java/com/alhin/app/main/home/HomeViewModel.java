package com.alhin.app.main.home;

import android.app.Application;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.JobObj;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeViewModel extends AndroidViewModel implements HomeModel.ModelCallback {
    private HomeModel model;
    private ViewListener viewListener;/* communicator betn view model and view */
    private ParentCommunication parentListener;/* communicator betn. fragment and its container */
    private ObservableField<Integer> searchProgress/* spinners progress */, listProgress/* rec. progress */;
    private ObservableField<Boolean> buttonsClickable/* flag to check if buttons are clickable or no */;
    private ObservableField<Integer> emptyListTextView/* flag to check if (no jobs available) is visible or not */;
    private ObservableField<String> searchErrorMessage/* error message of spinner */, listErrorMessage/* error message of rec. */;
    private ObservableField<Integer> searchErrorView/* flag to check the visibility of spinners error view */, listErrorView/* flag to check the visibility of rec. error view */;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        model = new HomeModel(application);
        searchProgress = new ObservableField<>(View.GONE);
        listProgress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        emptyListTextView = new ObservableField<>(View.GONE);
        searchErrorView = new ObservableField<>(View.GONE);
        searchErrorMessage = new ObservableField<>("");
        listErrorView = new ObservableField<>(View.GONE);
        listErrorMessage = new ObservableField<>("");
    }


    /*
        call model fun. to search for jobs (recommenced jobs)
        show progress dialog of recycler view then make req.
     */
    protected MutableLiveData<List<JobObj>> requestQuery() {
        setListProgress(View.VISIBLE);
        return model.search(this);
    }

    /*
        call model fun. to fetch countries to use for search
        show progress dialog of two spinners & disable any button then make req
     */
    protected MutableLiveData<List<Country>> requestCountries() {
        setSearchProgress(View.VISIBLE);
        setButtonsClickable(false);
        return model.fetchCountries(this);
    }


    /*
        call model fun. to fetch jobs titles to use for search
        show progress dialog of two spinners & disable any button then make req
     */
    protected MutableLiveData<List<JobTitle>> requestJobsTitles() {
        setSearchProgress(View.VISIBLE);
        setButtonsClickable(false);
        return model.fetchJobTitles(this);
    }

    /*
        handle error response of (countries, jobsTitles, recommended) reqs.
        @boolean isSearch flag to check if response comes from (countries, jobTitles) or (recommended)
     */
    @Override
    public void onFailureHandler(Throwable t, boolean isSearch) {
        if (isSearch) {
            setSearchErrorView(View.VISIBLE);
            if (t instanceof IOException)
                setSearchErrorMessage(getApplication().getString(R.string.no_internet_connection));
            else
                setSearchErrorMessage(getApplication().getString(R.string.error_fetching_data));
        } else {
            setListErrorView(View.VISIBLE);
            if (t instanceof IOException)
                setListErrorMessage(getApplication().getString(R.string.no_internet_connection));
            else
                setListErrorMessage(getApplication().getString(R.string.error_fetching_data));
        }

    }

    /*
        handle error response of fav/unFav req.
     */
    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    /*
        handle internal server error response of (countries, jobsTitles, recommended) reqs.
        @boolean isSearch flag to check if response comes from (countries, jobTitles) or (recommended)
     */
    @Override
    public void handleError500(String message, boolean isSearch) {
        if (isSearch) {
            setSearchErrorView(View.VISIBLE);
            setSearchErrorMessage(message);
        } else {
            setListErrorView(View.VISIBLE);
            setListErrorMessage(message);
        }
    }

    /*
        set user inputs in Map then navigate to Jobs Fragment to search
        for jobs depending on user inputs
     */
    protected void requestSearch(List<JobTitle> jobs, String jobId, String countryId) {
        Map<String, Object> map = new HashMap<>();
        if (jobId.equals("0") && countryId.equals("0")) {
            map.put("newer", true);
        } else {
            if (!jobId.equals("0"))
                map.put("job_title", jobId);
            if (!countryId.equals("0"))
                map.put("residence_country_id", countryId);
        }
        map.put("apiToken", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        map.put("language", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.english_key)));
        parentListener.sendQuery(jobs, map);
    }

    /*
        check what user clicked on (the whole item or fav icon)
        if fav icon -> make favourite req.
        else -> navigate to Jobs fragment to search and show the rslt.
     */
    public void handleOnJobItemClicked(View view, String jobId, boolean isFav) {
        switch (view.getId()) {
            case R.id.favourite:
                if (MySingleton.getInstance(getApplication()).isLoggedIn())
                    model.favUnFav(jobId, isFav, this);
                else
                    viewListener.showToastMessage(getApplication().getString(R.string.you_should_login_first));
                break;
            default:
                viewListener.viewJobInfo(jobId);
        }
    }

    /*
        call model fun. to fetch countries to use for search
        show progress dialog & disable any button then make req
     */
    public void onRetryClickListener(View view, boolean isSearch) {
        setButtonsClickable(false);
        if (isSearch) {
            setSearchProgress(View.VISIBLE);
            setSearchErrorView(View.GONE);
            model.fetchJobTitles(this);
            model.fetchCountries(this);
        } else {
            setListProgress(View.VISIBLE);
            setListErrorView(View.GONE);
            model.search(this);
        }
    }

    /*
        show the response/error of fav/unFav req.
     */
    @Override
    public void onFavUnFaveResponse(String message) {
        viewListener.showToastMessage(message);
    }


    /*
         Setters & Getters -------------> start
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public void setParentListener(ParentCommunication parentListener) {
        this.parentListener = parentListener;
    }

    public ObservableField<String> getSearchErrorMessage() {
        return searchErrorMessage;
    }

    protected void setSearchErrorMessage(String message) {
        searchErrorMessage.set(message);
    }

    public ObservableField<Integer> getSearchErrorView() {
        return searchErrorView;
    }

    protected void setSearchErrorView(int errorView) {
        this.searchErrorView.set(errorView);
    }

    public ObservableField<String> getListErrorMessage() {
        return listErrorMessage;
    }

    protected void setListErrorMessage(String message) {
        listErrorMessage.set(message);
    }

    public ObservableField<Integer> getListErrorView() {
        return listErrorView;
    }

    protected void setListErrorView(int errorView) {
        this.listErrorView.set(errorView);
    }

    public ObservableField<Integer> getSearchProgress() {
        return searchProgress;
    }

    public ObservableField<Integer> getListProgress() {
        return listProgress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setSearchProgress(int searchProgress) {
        this.searchProgress.set(searchProgress);
    }

    @Override
    public void setListProgress(int listProgress) {
        this.listProgress.set(listProgress);
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        buttonsClickable.set(clickable);
    }

    @Override
    public void setEmptyListTextView(int empty) {
        emptyListTextView.set(empty);
    }
    /* \\Setters & Getters ------------> end // */


    protected interface ViewListener {
        void showToastMessage(String message);

        void viewJobInfo(String jobId);
    }

    public interface ParentCommunication {
        void sendQuery(List<JobTitle> jobs, Map<String, Object> map);
    }

}
