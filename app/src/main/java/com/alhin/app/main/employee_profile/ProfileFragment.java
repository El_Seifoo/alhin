package com.alhin.app.main.employee_profile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.alhin.app.R;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.create_edit_cv.CrEdCvActivity;
import com.alhin.app.databinding.FragmentProfileBinding;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class ProfileFragment extends Fragment implements Presenter {
    protected static final int CREATE_EDIT_CV_REQUEST_CODE = 1;
    private ProfileViewModel viewModel;
    private FragmentProfileBinding dataBinding;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_profile, container, false);
        View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);


        viewModel.requestCv().observe(this, new Observer<CvInfo>() {
            @Override
            public void onChanged(CvInfo cvInfo) {
                dataBinding.setCv(cvInfo);
            }
        });

        return view;
    }

    @Override
    public void onCreateCvClicked() {
        startActivityForResult(new Intent(getContext(), CrEdCvActivity.class), CREATE_EDIT_CV_REQUEST_CODE);
    }

    @Override
    public void onEditClicked() {
        Intent intent = new Intent(getContext(), CrEdCvActivity.class);
        intent.putExtra("CV", dataBinding.getCv());
        startActivityForResult(intent, CREATE_EDIT_CV_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        viewModel.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
