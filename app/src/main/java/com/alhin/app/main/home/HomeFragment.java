package com.alhin.app.main.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alhin.app.R;
import com.alhin.app.application.ApplicationActivity;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.JobObj;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.databinding.FragmentHomeBinding;
import com.alhin.app.main.home.HomeViewModel.ParentCommunication;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

import java.util.List;

public class HomeFragment extends Fragment implements HomeViewModel.ViewListener, RecommendedJobsAdapter.OnItemClicked, Presenter {
    private HomeViewModel viewModel;
    private FragmentHomeBinding dataBinding;
    private Spinner locationSpinner, jobTitleSpinner;
    private LocationSpinnerAdapter locationAdapter;
    private JobsSpinnerAdapter jobsAdapter;
    private RecyclerView jobsList;
    private RecommendedJobsAdapter adapter;


    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_home, container, false);
        View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        viewModel.setViewListener(this);
        viewModel.setParentListener((ParentCommunication) getActivity());
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        jobsList = dataBinding.recommendedJobsList;
        jobsList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        adapter = new RecommendedJobsAdapter(this);


        locationSpinner = dataBinding.locationSpinner;
        jobTitleSpinner = dataBinding.jobTitleSpinner;

        viewModel.requestCountries().observe(this, new Observer<List<Country>>() {
            @Override
            public void onChanged(List<Country> countries) {
                countries.add(0, new Country("0", getString(R.string.location)));
                locationAdapter = new LocationSpinnerAdapter(countries, getContext(), false);
                locationSpinner.setAdapter(locationAdapter);
            }
        });

        viewModel.requestJobsTitles().observe(this, new Observer<List<JobTitle>>() {
            @Override
            public void onChanged(List<JobTitle> jobTitles) {
                jobTitles.add(0, new JobTitle("0", getString(R.string.job_title)));
                jobsAdapter = new JobsSpinnerAdapter(jobTitles, getContext(), false);
                jobTitleSpinner.setAdapter(jobsAdapter);

            }
        });

        viewModel.requestQuery().observe(this, new Observer<List<JobObj>>() {
            @Override
            public void onChanged(List<JobObj> jobObjs) {
                adapter.setJobsList(jobObjs);
                jobsList.setAdapter(adapter);
            }
        });

        return view;
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSearchButtonClicked() {
        viewModel.requestSearch(jobsAdapter.getJobs(), jobsAdapter == null ? "0" : jobsAdapter.getJobId(dataBinding.jobTitleSpinner.getSelectedItemPosition()),
                locationAdapter == null ? "0" : locationAdapter.getCountryId(dataBinding.locationSpinner.getSelectedItemPosition()));


    }

    @Override
    public void viewJobInfo(String jobId) {
        Intent intent = new Intent(getContext(), ApplicationActivity.class);
        intent.putExtra("jobId", jobId);
        startActivity(intent);
    }

    @Override
    public void onJobItemClicked(View view, String jobId, boolean isFav) {
        viewModel.handleOnJobItemClicked(view, jobId, isFav);
    }

}
