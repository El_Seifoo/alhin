package com.alhin.app.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.LayoutDirection;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.main.home.HomeFragment;
import com.alhin.app.main.home.HomeViewModel;
import com.alhin.app.main.jobs.JobsViewModel;
import com.alhin.app.notifications.NotificationsActivity;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;
import com.alhin.app.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements MainViewModel.ViewListener, BottomNavigationView.OnNavigationItemSelectedListener, HomeViewModel.ParentCommunication, JobsViewModel.ParentCommunication {
    private NavigationView navigationView;
    private View navHeader;
    private DrawerLayout drawer;
    private BottomNavigationView bottomNavigation;
    private Toolbar toolbar;

    // tag for attached fragments
    protected static final String TAG_HOME = "home";//0
    protected static final String TAG_CV_LIST = "cvList";//1
    protected static final String TAG_JOBS_LIST = "jobsList";//2
    protected static final String TAG_PROFILE = "profile";//2

    // index to know the selected nav nav_menu item
    public int naveItemIndex = 0;

    // tag for the current attached fragment
    protected static String CURRENT_TAG = TAG_HOME;

    // flag to make decision when user press back button
    private boolean shouldLoadHomeFragOnBackPress = false;

    private Handler mHandler;
    private Runnable runnable;

    private MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        viewModel.setViewListener(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        bottomNavigation = findViewById(R.id.bottom_navigation);
        if (!MySingleton.getInstance(this).isLoggedIn())
            bottomNavigation.getMenu().getItem(3).setEnabled(false);
        bottomNavigation.setOnNavigationItemSelectedListener(this);

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navHeader = navigationView.getHeaderView(0);

        headerPic = navHeader.findViewById(R.id.profile_pic);
        username = navHeader.findViewById(R.id.name);

        // load navigation drawer header view
        viewModel.loadHeader(navigationView, navHeader, username);
        // initialize navigation drawer
        setUpNavigation();

        viewModel.checkSavedInstanceState(savedInstanceState);
    }

    private void setUpNavigation() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return viewModel.handleOnNavigationItemSelected(menuItem);

            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                FrameLayout frame = (FrameLayout) findViewById(R.id.frame_container);
                if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL)
                    frame.setX(-slideOffset * drawerView.getWidth());
                else frame.setX(slideOffset * drawerView.getWidth());
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.addDrawerListener(actionBarDrawerToggle);


        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void loadHomeFragment() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.nav_item_activity_titles)[naveItemIndex]);

        // close navigation drawer if user selected the current navigation item
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            closeDrawer();
            return;
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = viewModel.getSelectedFragment(naveItemIndex, map, jobs);
                if (fragment != null) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.fade_in,
                            android.R.anim.fade_out);
                    transaction.replace(R.id.frame, fragment, CURRENT_TAG);
                    transaction.commitAllowingStateLoss();
                }
            }
        };

        if (runnable != null)
            mHandler.post(runnable);

        // close navigation drawer
        closeDrawer();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    @Override
    public boolean navigateToActivity(Class destination) {
        startActivity(new Intent(this, destination));
        closeDrawer();
        return true;
    }


    @Override
    public boolean handleLogoutAction(Class destination) {
        finish();
        startActivity(new Intent(this, destination));
        closeDrawer();
        return true;
    }

    @Override
    public void closeDrawer() {
        drawer.closeDrawers();
    }

    @Override
    public void handleNavFragments(int naveItemIndex, String currentTag, boolean shouldLoadHomeFragOnBackPress) {
        this.naveItemIndex = naveItemIndex;
        CURRENT_TAG = currentTag;
        this.shouldLoadHomeFragOnBackPress = shouldLoadHomeFragOnBackPress;
    }

    @Override
    public void setProfilePic(String url) {
        Glide.with(this).load(URLs.BASE + url)
                .error(R.mipmap.logo)
                .into(headerPic);
    }

    @Override
    public void selectHome() {
        bottomNavigation.setSelectedItemId(R.id.bottom_nav_home);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void languageChangedSuccessfully() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        return;
    }


    @Override
    public void onBackPressed() {

        if (!viewModel.onBackBtnPressed(drawer.isDrawerOpen(GravityCompat.START), shouldLoadHomeFragOnBackPress))
            super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return viewModel.handleOnNavigationItemSelected(menuItem);
    }

    Map<String, Object> map;
    List<JobTitle> jobs;

    @Override
    public void sendQuery(List<JobTitle> jobs, Map<String, Object> map) {
        this.map = map;
        this.jobs = jobs;
        bottomNavigation.setSelectedItemId(R.id.bottom_nav_jobs_list);
    }

    @Override
    public void clearArgs() {
        map = null;
        jobs = null;
    }

    private CircleImageView headerPic;
    private TextView username;

    @Override
    protected void onRestart() {
        super.onRestart();
        viewModel.loadHeader(navigationView, navHeader, username);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (naveItemIndex == 0 && MySingleton.getInstance(this).isLoggedIn())
            getMenuInflater().inflate(R.menu.option_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.option_notification) {
            return navigateToActivity(NotificationsActivity.class);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
