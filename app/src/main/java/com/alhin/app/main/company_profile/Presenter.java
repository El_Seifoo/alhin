package com.alhin.app.main.company_profile;

public interface Presenter {
    void onPickPicClicked();

    void onPublishButtonClicked();
}
