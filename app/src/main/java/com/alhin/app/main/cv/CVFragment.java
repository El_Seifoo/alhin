package com.alhin.app.main.cv;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alhin.app.R;
import com.alhin.app.classes.CvObj;
import com.alhin.app.classes.JobObj;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.cvInfo.CvInfoActivity;
import com.alhin.app.databinding.FragmentCvBinding;
import com.alhin.app.main.home.JobsSpinnerAdapter;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

import java.util.List;

public class CVFragment extends Fragment implements CVsViewModel.ViewListener, CVsAdapter.OnItemClicked {
    private CVsViewModel viewModel;
    private FragmentCvBinding dataBinding;
    private CVsAdapter adapter;
    private Spinner jobsTitlesSpinner;
    private JobsSpinnerAdapter jobsTitlesAdapter;

    public static CVFragment newInstance() {
        return new CVFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_cv, container, false);
        View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(CVsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);


        dataBinding.cvsList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapter = new CVsAdapter(this);

        viewModel.requestQuery(null).observe(this, new Observer<List<CvObj>>() {
            @Override
            public void onChanged(List<CvObj> list) {
                adapter.setCvsList(list);
                dataBinding.cvsList.setAdapter(adapter);
            }
        });

        jobsTitlesSpinner = dataBinding.jobTitleSpinner;
        jobsTitlesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewModel.requestQuery(jobsTitlesAdapter.getJobName(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        viewModel.requestJobsTitles().observe(this, new Observer<List<JobTitle>>() {
            @Override
            public void onChanged(List<JobTitle> jobTitles) {
                jobTitles.add(0, new JobTitle("0", getString(R.string.job_title)));
                jobsTitlesAdapter = new JobsSpinnerAdapter(jobTitles, getContext(),false);
                jobsTitlesSpinner.setAdapter(jobsTitlesAdapter);
            }
        });

        return view;
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void viewCvInfo(String cvId) {
        Intent intent = new Intent(getContext(), CvInfoActivity.class);
        intent.putExtra("cvId", cvId);
        startActivity(intent);
    }

    @Override
    public void onCvItemClicked(View view, String cvId, boolean isFav) {
        viewModel.handleOnCvItemClicked(view, cvId, isFav);
    }
}
