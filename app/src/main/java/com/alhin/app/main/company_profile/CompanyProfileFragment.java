package com.alhin.app.main.company_profile;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.alhin.app.R;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.databinding.FragmentCompanyProfileBinding;
import com.alhin.app.main.home.JobsSpinnerAdapter;
import com.alhin.app.main.home.LocationSpinnerAdapter;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.List;

public class CompanyProfileFragment extends Fragment implements ComProfileViewModel.ViewListener, Presenter {
    protected static final int PICK_PICTURE_REQUEST_CODE = 1;
    private ComProfileViewModel viewModel;
    private FragmentCompanyProfileBinding dataBinding;
    private Spinner locationSpinner, jobTitleSpinner;
    private LocationSpinnerAdapter locationAdapter;
    private JobsSpinnerAdapter jobsAdapter;

    public static CompanyProfileFragment newInstance() {
        return new CompanyProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_company_profile, container, false);
        View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(ComProfileViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        locationSpinner = dataBinding.country;
        jobTitleSpinner = dataBinding.jobTitle;

        viewModel.requestCountries().observe(this, new Observer<List<Country>>() {
            @Override
            public void onChanged(List<Country> countries) {
                locationAdapter = new LocationSpinnerAdapter(countries, getContext(), true);
                locationSpinner.setAdapter(locationAdapter);
            }
        });

        viewModel.requestJobsTitles().observe(this, new Observer<List<JobTitle>>() {
            @Override
            public void onChanged(List<JobTitle> jobTitles) {
                jobsAdapter = new JobsSpinnerAdapter(jobTitles, getContext(), true);
                jobTitleSpinner.setAdapter(jobsAdapter);

            }
        });

        return view;
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void startPicking(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void startCroppingFrag(Uri uri) {
        CropImage.activity(uri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(14, 10)
                .start(getContext(), this);
    }

    @Override
    public void showEditTextError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
    }

    @Override
    public void clearAllUserInputs() {
        dataBinding.companyName.setText("");
        dataBinding.phoneNumber.setText("");
        dataBinding.email.setText("");
        dataBinding.salary.setText("");
        dataBinding.experience.setText("");
        viewModel.setPickedPicture("");
        dataBinding.jobTitle.setSelection(0);
        dataBinding.country.setSelection(0);
    }

    @Override
    public void onPickPicClicked() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PICK_PICTURE_REQUEST_CODE
        );
    }

    @Override
    public void onPublishButtonClicked() {
//        String jobTitle, String companyName, String image, double salary, int experience, String phone, String email, String token, String countryId
        viewModel.requestPublish(jobsAdapter.getJobId(dataBinding.jobTitle.getSelectedItemPosition()),
                dataBinding.companyName, dataBinding.salary, dataBinding.experience, dataBinding.phoneNumber, dataBinding.email,
                locationAdapter.getCountryId(dataBinding.country.getSelectedItemPosition()));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        viewModel.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
