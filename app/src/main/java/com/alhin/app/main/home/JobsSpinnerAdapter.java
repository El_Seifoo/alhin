package com.alhin.app.main.home;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alhin.app.R;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.JobTitle;

import java.util.ArrayList;
import java.util.List;

public class JobsSpinnerAdapter extends BaseAdapter {
    private List<JobTitle> jobs;
    private Context context;
    private LayoutInflater inflater;
    //flag to check if spinner in (home, cvs List , jobs List) or (profile)
    // if spinner in profile remove the title ..
    private boolean profile;

    public JobsSpinnerAdapter(List<JobTitle> jobs, Context context, boolean profile) {
        this.jobs = jobs;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.profile = profile;
    }

    public String getJobId(int index) {
        return jobs.get(index).getId();
    }

    public String getJobName(int index) {
        return jobs.get(index).getName();
    }

    public int getJobPosition(String id) {
        for (int i = 0; i < jobs.size(); i++) {
            if (jobs.get(i).getId().equals(id)) {
                return i;
            }
        }
        return 0;
    }

    public int getJobPositionByTitle(String jobTitle) {
        for (int i = 0; i < jobs.size(); i++) {
            if (jobs.get(i).getName().equals(jobTitle)) {
                return i;
            }
        }
        return 0;
    }


    public List<JobTitle> getJobs() {
        return jobs != null ? jobs : new ArrayList<JobTitle>();
    }

    @Override
    public int getCount() {
        return jobs.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item, null);
        }


        TextView textView = view.findViewById(R.id.spinner_text);
        if (!profile && position == 0) {
            textView.setTextColor(Color.GRAY);
        }
        textView.setText(jobs.get(position).getName());

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item, null);
        }

        TextView textView = view.findViewById(R.id.spinner_text);
        if (!profile && position == 0) {
            textView.setTextColor(Color.GRAY);
        }
        textView.setText(jobs.get(position).getName());

        return view;
    }


}
