package com.alhin.app.main.employee_profile;

public interface Presenter {
    void onCreateCvClicked();

    void onEditClicked();
}
