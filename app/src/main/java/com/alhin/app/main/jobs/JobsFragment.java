package com.alhin.app.main.jobs;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alhin.app.R;
import com.alhin.app.application.ApplicationActivity;
import com.alhin.app.classes.JobObj;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.databinding.FragmentJobsBinding;
import com.alhin.app.main.home.JobsSpinnerAdapter;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JobsFragment extends Fragment implements JobsViewModel.ViewListener, JobsAdapter.OnItemClicked {
    private JobsViewModel viewModel;
    private FragmentJobsBinding dataBinding;
    private JobsAdapter adapter;
    private Spinner jobsTitlesSpinner;
    private JobsSpinnerAdapter jobsTitlesAdapter;

    public static JobsFragment newInstance(List<JobTitle> jobs, HashMap<String, Object> map) {
        JobsFragment fragment = new JobsFragment();

        if (jobs != null) {
            Bundle args = new Bundle();
            args.putSerializable("map", map);
            args.putSerializable("jobs", (ArrayList<JobTitle>) jobs);
            fragment.setArguments(args);
        }


        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Language.setLanguage(getContext(), MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_jobs, container, false);
        View view = dataBinding.getRoot();
        viewModel = ViewModelProviders.of(this).get(JobsViewModel.class);
        viewModel.setViewListener(this);
        viewModel.setParentListener((JobsViewModel.ParentCommunication) getActivity());
        dataBinding.setViewModel(viewModel);

        jobsTitlesSpinner = dataBinding.jobTitleSpinner;

        dataBinding.jobsList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapter = new JobsAdapter(this);


        viewModel.requestQuery(null).observe(this, new Observer<List<JobObj>>() {
            @Override
            public void onChanged(List<JobObj> list) {
                adapter.setJobsList(list);
                dataBinding.jobsList.setAdapter(adapter);
            }
        });


        if (getArguments() != null) {
            jobsTitlesAdapter = new JobsSpinnerAdapter((List<JobTitle>) getArguments().getSerializable("jobs"), getContext(), false);
            jobsTitlesSpinner.setAdapter(jobsTitlesAdapter);
            HashMap<String, Object> map = (HashMap<String, Object>) getArguments().getSerializable("map");
            jobsTitlesSpinner.setSelection(jobsTitlesAdapter.getJobPosition(map.containsKey("newer") ? "0" : (map.containsKey("job_title") ? map.get("job_title").toString() : "0")));
            viewModel.requestQuery(map);

        } else {
            viewModel.requestJobsTitles().observe(this, new Observer<List<JobTitle>>() {
                @Override
                public void onChanged(List<JobTitle> jobTitles) {
                    jobTitles.add(0, new JobTitle("0", getString(R.string.job_title)));
                    jobsTitlesAdapter = new JobsSpinnerAdapter(jobTitles, getContext(), false);
                    jobsTitlesSpinner.setAdapter(jobsTitlesAdapter);
                }
            });
        }

        jobsTitlesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewModel.handleMapToRequestQuery(jobsTitlesAdapter.getJobId(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return view;
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void viewJobInfo(String jobId) {
        Intent intent = new Intent(getContext(), ApplicationActivity.class);
        intent.putExtra("jobId", jobId);
        startActivity(intent);
    }

    @Override
    public void onJobItemClicked(View view, String jobId, boolean isFav) {
        viewModel.handleOnJobItemClicked(view, jobId, isFav);
    }
}
