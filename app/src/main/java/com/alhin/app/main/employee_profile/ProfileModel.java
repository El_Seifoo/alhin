package com.alhin.app.main.employee_profile;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileModel {
    private Context context;

    public ProfileModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<CvInfo> cvInfoMutableLiveData = new MutableLiveData<>();


    protected MutableLiveData<CvInfo> fetchCv(Map<String, String> map, final ModelCallback callback) {
        Call<ApiResponse<CvInfo>> call = MySingleton.getInstance(context).createService().getCv(map);
        call.enqueue(new Callback<ApiResponse<CvInfo>>() {
            @Override
            public void onResponse(Call<ApiResponse<CvInfo>> call, Response<ApiResponse<CvInfo>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponseError(context.getString(R.string.something_went_wrong));
                    return;
                }

                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        cvInfoMutableLiveData.setValue(response.body().getData());
                        callback.setCreateNewCv(View.GONE);
                    } else {
                        if (response.body().getMessage().trim().equals(context.getString(R.string.response_is_empty)))
                            callback.setCreateNewCv(View.VISIBLE);
                        else callback.handleResponseError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<CvInfo>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });

        return cvInfoMutableLiveData;
    }


    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void setCreateNewCv(int create);

        void handleResponseError(String message);

        void onFailureHandler(Throwable t);
    }
}
