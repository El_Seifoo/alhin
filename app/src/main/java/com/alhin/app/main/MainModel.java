package com.alhin.app.main;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainModel {
    private Context context;

    public MainModel(Context context) {
        this.context = context;
    }


    protected void changeLang(Map<String, String> map, final ModelCallback callback) {
        Log.e("map",map.toString());
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().changeLang(map);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 500) {
                    callback.handleResponse(context.getString(R.string.something_went_wrong));
                    return;
                }

                if (response.code() == 200) {
                    callback.handleResponse(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void onFailureHandler(Throwable t);

        void handleResponse(String message);
    }
}
