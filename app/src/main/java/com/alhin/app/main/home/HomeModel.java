package com.alhin.app.main.home;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.ApiListResponse;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.JobObj;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeModel {
    private Context context;
    private boolean countriesFetched = false, jobsTitlesFetched = false;

    public HomeModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<Country>> countriesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Country>> fetchCountries(final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE,
                context.getString(R.string.english_key)));
        Call<ApiListResponse<Country>> call = MySingleton.getInstance(context).createService().countries(map);
        call.enqueue(new Callback<ApiListResponse<Country>>() {
            @Override
            public void onResponse(Call<ApiListResponse<Country>> call, Response<ApiListResponse<Country>> response) {

                if (response.code() == 500) {
                    callback.setSearchProgress(View.GONE);
                    countriesFetched = false;
                    callback.setButtonsClickable(true);
                    callback.handleError500(context.getString(R.string.something_went_wrong), true);
                    return;
                }


                if (response.code() == 200) {
                    if (jobsTitlesFetched) {
                        callback.setSearchProgress(View.GONE);
                        callback.setButtonsClickable(true);
                    }
                    countriesFetched = true;
                    countriesMutableLiveData.setValue(response.body().getData());
                    return;
                }

                // for any other status code not handled
                callback.setSearchProgress(View.GONE);
                callback.setButtonsClickable(true);
                countriesFetched = false;
            }

            @Override
            public void onFailure(Call<ApiListResponse<Country>> call, Throwable t) {
                callback.setSearchProgress(View.GONE);
                callback.setButtonsClickable(true);
                countriesFetched = false;
                callback.onFailureHandler(t, true);
            }
        });

        return countriesMutableLiveData;
    }

    private MutableLiveData<List<JobTitle>> jobTitlesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<JobTitle>> fetchJobTitles(final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE,
                context.getString(R.string.english_key)));
        Call<ApiListResponse<JobTitle>> call = MySingleton.getInstance(context).createService().jobsTitles(map);
        call.enqueue(new Callback<ApiListResponse<JobTitle>>() {
            @Override
            public void onResponse(Call<ApiListResponse<JobTitle>> call, Response<ApiListResponse<JobTitle>> response) {
                if (response.code() == 500) {
                    if (countriesFetched) {
                        callback.setSearchProgress(View.GONE);
                        callback.setButtonsClickable(true);
                    }
                    jobsTitlesFetched = false;
                    callback.handleError500(context.getString(R.string.something_went_wrong), true);
                    return;
                }
                if (response.code() == 200) {
                    if (countriesFetched) {
                        callback.setSearchProgress(View.GONE);
                        callback.setButtonsClickable(true);
                    }
                    jobsTitlesFetched = true;
                    jobTitlesMutableLiveData.setValue(response.body().getData());
                    return;
                }

                // for any other status code not handled
                callback.setSearchProgress(View.GONE);
                callback.setButtonsClickable(true);
                jobsTitlesFetched = false;
            }

            @Override
            public void onFailure(Call<ApiListResponse<JobTitle>> call, Throwable t) {
                callback.setSearchProgress(View.GONE);
                callback.setButtonsClickable(true);
                jobsTitlesFetched = false;
                callback.onFailureHandler(t, true);
            }
        });
        return jobTitlesMutableLiveData;
    }

    private MutableLiveData<List<JobObj>> jobsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<JobObj>> search(final ModelCallback callback) {
        Map<String, Object> query = new HashMap<>();
        query.put("apiToken", MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        query.put("newer", true);
        query.put("language", MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE, context.getString(R.string.english_key)));
        Call<ApiListResponse<JobObj>> call = MySingleton.getInstance(context).createService().search(query);
        call.enqueue(new Callback<ApiListResponse<JobObj>>() {
            @Override
            public void onResponse(Call<ApiListResponse<JobObj>> call, Response<ApiListResponse<JobObj>> response) {
                callback.setListProgress(View.GONE);
                if (response.code() == 500) {
                    callback.handleError500(context.getString(R.string.something_went_wrong), false);
                    return;
                }
                if (response.code() == 200) {
                    List<JobObj> jobs = response.body().getData();
                    if (jobs == null)
                        callback.setEmptyListTextView(View.VISIBLE);
                    else if (jobs.isEmpty())
                        callback.setEmptyListTextView(View.VISIBLE);
                    else {
                        callback.setEmptyListTextView(View.GONE);
                        jobsMutableLiveData.setValue(response.body().getData());
                    }
                }

            }

            @Override
            public void onFailure(Call<ApiListResponse<JobObj>> call, Throwable t) {
                callback.setListProgress(View.GONE);
                callback.onFailureHandler(t, false);
            }
        });

        return jobsMutableLiveData;
    }

    protected void favUnFav(final String jobId, final boolean isFav, final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("apiToken", MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        map.put("job_id", jobId);
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().favUnFav(map);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                if (response.code() == 401) {
                    Log.e("401", "400000000000001");
                    return;
                }
                if (response.code() == 500) {
                    callback.onFavUnFaveResponse(context.getString(R.string.something_went_wrong));
                    return;
                }

                if (response.code() == 200) {
                    if (response.body().getMessage().trim().toLowerCase().equals(context.getString(R.string.done)))
                        jobsMutableLiveData.getValue().get(findObjectById(jobsMutableLiveData.getValue(), jobId)).setFavourite(!isFav);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.onFailureHandler(t);
            }
        });
    }

    private int findObjectById(List<JobObj> list, String id) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().equals(id))
                return i;
        }

        return 0;
    }

    protected interface ModelCallback {
        void setSearchProgress(int searchProgress);

        void setListProgress(int listProgress);

        void setButtonsClickable(boolean clickable);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t, boolean isSearch);

        void onFailureHandler(Throwable t);

        void handleError500(String message, boolean isSearch);

        void onFavUnFaveResponse(String message);
    }
}
