package com.alhin.app.main.home;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alhin.app.R;
import com.alhin.app.classes.Country;

import java.util.List;

public class LocationSpinnerAdapter extends BaseAdapter {
    private List<Country> countries;
    private Context context;
    private LayoutInflater inflater;
    //flag to check if spinner in (home, cvs List , jobs List) or (profile)
    // if spinner in profile remove the title ..
    private boolean profile;

    public LocationSpinnerAdapter(List<Country> countries, Context context, boolean profile) {
        this.countries = countries;
        this.context = context;
        this.profile = profile;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public String getCountryId(int index) {
        return countries.get(index).getId();
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item, null);
        }


        TextView textView = view.findViewById(R.id.spinner_text);
        if (!profile && position == 0) {
            textView.setTextColor(Color.GRAY);
        }
        textView.setText(countries.get(position).getName());

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item, null);
        }

        TextView textView = view.findViewById(R.id.spinner_text);
        if (!profile && position == 0) {
            textView.setTextColor(Color.GRAY);
        }
        textView.setText(countries.get(position).getName());

        return view;
    }
}
