package com.alhin.app.main.cv;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.alhin.app.R;
import com.alhin.app.classes.CvObj;
import com.alhin.app.databinding.CvsListItemBinding;

import java.util.List;

public class CVsAdapter extends RecyclerView.Adapter<CVsAdapter.Holder> {
    private List<CvObj> cvsList;
    private final OnItemClicked listener;

    public CVsAdapter(OnItemClicked listener) {
        this.listener = listener;
    }

    public void setCvsList(List<CvObj> cvsList) {
        this.cvsList = cvsList;
        notifyDataSetChanged();
    }

    protected interface OnItemClicked {
        void onCvItemClicked(View view, String cvId, boolean isFav);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((CvsListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.cvs_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.dataBinding.setCv(cvsList.get(position));
    }

    @Override
    public int getItemCount() {
        return cvsList != null ? cvsList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CvsListItemBinding dataBinding;
        public Holder(@NonNull CvsListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
            dataBinding.viewButton.setOnClickListener(this);
            dataBinding.favourite.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onCvItemClicked(v, dataBinding.getCv().getId(), dataBinding.getCv().isFavourite());
        }
    }
}
