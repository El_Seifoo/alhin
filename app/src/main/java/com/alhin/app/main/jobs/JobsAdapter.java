package com.alhin.app.main.jobs;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.alhin.app.R;
import com.alhin.app.classes.JobObj;
import com.alhin.app.databinding.JobsListItemBinding;

import java.util.List;

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.Holder> {
    private List<JobObj> jobsList;
    private final OnItemClicked listener;

    public JobsAdapter(OnItemClicked listener) {
        this.listener = listener;
    }

    public void setJobsList(List<JobObj> jobsList) {
        this.jobsList = jobsList;
        notifyDataSetChanged();
    }

    protected interface OnItemClicked {
        void onJobItemClicked(View view, String jobId, boolean isFav);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((JobsListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.jobs_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.dataBinding.setJob(jobsList.get(position));
    }

    @Override
    public int getItemCount() {
        return jobsList != null ? jobsList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private JobsListItemBinding dataBinding;

        public Holder(@NonNull JobsListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
            dataBinding.applyButton.setOnClickListener(this);
            dataBinding.favourite.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onJobItemClicked(v, dataBinding.getJob().getId(), dataBinding.getJob().isFavourite());
        }
    }
}
