package com.alhin.app.main.home;

public interface Presenter {
    void onSearchButtonClicked();
}
