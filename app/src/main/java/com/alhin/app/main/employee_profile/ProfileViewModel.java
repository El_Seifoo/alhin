package com.alhin.app.main.employee_profile;

import android.app.Application;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class ProfileViewModel extends AndroidViewModel implements ProfileModel.ModelCallback {
    private ProfileModel model;
    private ObservableField<Integer> progress/* progress */;
    private ObservableField<Integer> createNewCv/* flag to show create new cv btn if user doesn't have a cv */;
    private ObservableField<String> errorMessage/* error message of the view */;
    private ObservableField<Integer> errorView/* flag to check the visibility of the error view */;
    private ObservableField<Boolean> buttonsClickable;/* flag to make view btns clickable or not */

    public ProfileViewModel(@NonNull Application application) {
        super(application);
        model = new ProfileModel(application);
        progress = new ObservableField<>(View.GONE);
        createNewCv = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        buttonsClickable = new ObservableField<>(true);
    }


    /*
        call model fun. to fetch getCv by its id
        show progress dialog then make req
     */
    private Map<String, String> map;

    protected MutableLiveData<CvInfo> requestCv() {
        Map<String, String> map = new HashMap<>();
        map.put("apiToken", MySingleton.getInstance(getApplication())
                .getStringFromSharedPref(Constants.USER_TOKEN, ""));
        this.map = map;
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        return model.fetchCv(map, this);
    }


    /*
        call model fun. to fetch getCv by its id
        show progress dialog then make req
     */
    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        setErrorView(View.GONE);
        model.fetchCv(map, this);
    }

    /*
        handle internal server error or any message response of getCv req.
     */
    @Override
    public void handleResponseError(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        handle error response of getCv req.
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        setErrorMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ProfileFragment.CREATE_EDIT_CV_REQUEST_CODE && resultCode == RESULT_OK) {
            this.requestCv();
        }
    }

    // Setters & Getters --------> start
    public ObservableField<Integer> getCreateNewCv() {
        return createNewCv;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    @Override
    public void setCreateNewCv(int create) {
        this.createNewCv.set(create);
    }
    /* \\Setters & Getters ------------> end // */
}
