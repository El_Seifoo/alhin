package com.alhin.app.main;

import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;

import com.alhin.app.R;
import com.alhin.app.about_us.AboutUsActivity;
import com.alhin.app.applied_for.AppliedForActivity;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.classes.UserInfo;
import com.alhin.app.contact.ContactUsActivity;
import com.alhin.app.favourites.FavouritesActivity;
import com.alhin.app.login.LoginActivity;
import com.alhin.app.main.company_profile.CompanyProfileFragment;
import com.alhin.app.main.cv.CVFragment;
import com.alhin.app.main.home.HomeFragment;
import com.alhin.app.main.jobs.JobsFragment;
import com.alhin.app.main.employee_profile.ProfileFragment;
import com.alhin.app.terms_cond.TermsAndConditionsActivity;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;
import com.google.android.material.navigation.NavigationView;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainViewModel extends AndroidViewModel implements MainModel.ModelCallback {
    private ViewListener viewListener;
    private MainModel model;
    private ObservableField<Integer> progress;

    public MainViewModel(@NonNull Application application) {
        super(application);
        model = new MainModel(application);
        progress = new ObservableField<>(View.GONE);
    }

    protected void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    protected void checkSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            viewListener.handleNavFragments(0, MainActivity.TAG_HOME, false);
            viewListener.loadHomeFragment();
        }
    }

    /*
    nav_home 0
    nav_applied_for 1
    nav_language 2
    nav_favourite 3
    nav_contact_us 4
    nav_about_us 5
    nav_terms_conditions 6
    nav_log_out 7
    nav_log_int 8
    */
    protected void loadHeader(NavigationView navigationView, View navHeader, TextView username) {
        for (int i = 1; i < 5; i++) {
            if (i == 2)
                navigationView.getMenu().getItem(i).setActionView(R.layout.nav_menu_lang);
            else
                navigationView.getMenu().getItem(i).setActionView(R.layout.nav_menu_arrow);
        }
        if (MySingleton.getInstance(navigationView.getContext()).isLoggedIn()) {
            UserInfo user = MySingleton.getInstance(getApplication()).userData();
            viewListener.setProfilePic(user.getProfPic());
            username.setText(user.getName());
            navigationView.getMenu().getItem(8).setVisible(false);
        } else {
            navigationView.getMenu().getItem(7).setVisible(false);
            username.setVisibility(View.INVISIBLE);
        }
    }

    protected boolean handleOnNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.bottom_nav_cv_list:
                viewListener.handleNavFragments(1, MainActivity.TAG_CV_LIST, true);
                break;
            case R.id.bottom_nav_jobs_list:
                viewListener.handleNavFragments(2, MainActivity.TAG_JOBS_LIST, true);
                break;
            case R.id.bottom_nav_profile:
                viewListener.handleNavFragments(3, MainActivity.TAG_PROFILE, true);
                break;
            case R.id.nav_applied_for:
                if (MySingleton.getInstance(getApplication()).isLoggedIn())
                    if (MySingleton.getInstance(getApplication()).getBooleanFromSharedPref(Constants.IS_EMPLOYEE, true))
                        return viewListener.navigateToActivity(AppliedForActivity.class);
                viewListener.closeDrawer();
                break;
            case R.id.nav_language:
                //TODO(2) language view ..!!
                if (MySingleton.getInstance(getApplication()).isLoggedIn()) {
                    Map<String, String> map = new HashMap<>();
                    map.put("lang",
                            MySingleton.getInstance(getApplication())
                                    .getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.english_key))
                                    .equals(getApplication().getString(R.string.english_key)) ?
                                    getApplication().getString(R.string.arabic_key) :
                                    getApplication().getString(R.string.english_key));
                    map.put(MySingleton.getInstance(getApplication())
                                    .getBooleanFromSharedPref(Constants.IS_EMPLOYEE, true) ?
                                    "employeeApiToken" : "companyApiToken",
                            MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
                    progress.set(View.VISIBLE);
                    model.changeLang(map, this);
                } else {
                    // this is the last step before changing language if user is logged in
                    handleResponse(getApplication().getString(R.string.done_reverse_translatable));
                }

                break;
            case R.id.nav_favourite:
                if (MySingleton.getInstance(getApplication()).isLoggedIn())
                    return viewListener.navigateToActivity(FavouritesActivity.class);
                viewListener.closeDrawer();
                break;
            case R.id.nav_contact_us:
                return viewListener.navigateToActivity(ContactUsActivity.class);
            case R.id.nav_about_us:
                return viewListener.navigateToActivity(AboutUsActivity.class);
            case R.id.nav_terms_conditions:
                return viewListener.navigateToActivity(TermsAndConditionsActivity.class);
            case R.id.nav_login:
                return viewListener.navigateToActivity(LoginActivity.class);
            case R.id.nav_logout:
                MySingleton.getInstance(getApplication()).logout();
                return viewListener.handleLogoutAction(MainActivity.class);
            case R.id.bottom_nav_home:
            case R.id.nav_home:
            default:
                // navItemIndex , CURRENT_TAG
                viewListener.handleNavFragments(0, MainActivity.TAG_HOME, false);
                break;
        }

        if (!MySingleton.getInstance(getApplication()).isLoggedIn() &&
                (menuItem.getItemId() == R.id.bottom_nav_profile
                        || menuItem.getItemId() == R.id.nav_applied_for
                        || menuItem.getItemId() == R.id.nav_favourite)) {
            viewListener.showToastMessage(getApplication().getString(R.string.you_should_login_first));
            return true;
        }
        if (!MySingleton.getInstance(getApplication()).getBooleanFromSharedPref(Constants.IS_EMPLOYEE, true) &&
                menuItem.getItemId() == R.id.nav_applied_for) {
            viewListener.showToastMessage(getApplication().getString(R.string.only_employee_users_can_see_this));
            return true;
        }
        viewListener.loadHomeFragment();
        return true;
    }

    protected Fragment getSelectedFragment(int naveItemIndex, Map<String, Object> map, List<JobTitle> jobs) {
        switch (naveItemIndex) {
            case 1:
                return CVFragment.newInstance();
            case 2:
                return JobsFragment.newInstance(jobs, (HashMap<String, Object>) map);
            case 3:
                if (MySingleton.getInstance(getApplication()).getBooleanFromSharedPref(Constants.IS_EMPLOYEE, true))
                    return ProfileFragment.newInstance();
                else return CompanyProfileFragment.newInstance();
            case 0:
            default:
                return HomeFragment.newInstance();
        }
    }


    public boolean onBackBtnPressed(boolean drawerOpen, boolean shouldLoadHomeFragOnBackPress) {
        if (drawerOpen) {
            viewListener.closeDrawer();
            return true;
        }

        // return to home fragment if user pressed back in other fragment
        if (shouldLoadHomeFragOnBackPress) {
            viewListener.selectHome();
            return true;
        }

        return false;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_changing_language));
    }

    @Override
    public void handleResponse(String message) {
        if (message.trim().toLowerCase().equals(getApplication().getString(R.string.done_reverse_translatable))) {
            if (MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.english_key))
                    .equals(getApplication().getString(R.string.english_key))) {
                Language.setLanguage(getApplication(), getApplication().getString(R.string.arabic_key));
                MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.arabic_key));
            } else {
                Language.setLanguage(getApplication(), getApplication().getString(R.string.english_key));
                MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.english_key));
            }

            viewListener.languageChangedSuccessfully();
            return;
        }
        viewListener.showToastMessage(message);
    }


    protected interface ViewListener {
        void loadHomeFragment();

        boolean navigateToActivity(Class destination);

        boolean handleLogoutAction(Class destination);

        void closeDrawer();

        void handleNavFragments(int naveItemIndex, String currentTag, boolean shouldLoadHomeFragOnBackPress);

        void setProfilePic(String url);

        void selectHome();


        void showToastMessage(String message);

        void languageChangedSuccessfully();
    }
}
