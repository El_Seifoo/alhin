package com.alhin.app.main.company_profile;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.ApiListResponse;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.JobCreationObj;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;
import com.alhin.app.utils.ProgressRequestBody;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComProfileModel {
    private Context context;
    private boolean countriesFetched = false, jobsTitlesFetched = false;

    public ComProfileModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<Country>> countriesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Country>> fetchCountries(final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE,
                context.getString(R.string.english_key)));
        Call<ApiListResponse<Country>> call = MySingleton.getInstance(context).createService().countries(map);
        call.enqueue(new Callback<ApiListResponse<Country>>() {
            @Override
            public void onResponse(Call<ApiListResponse<Country>> call, Response<ApiListResponse<Country>> response) {

                if (response.code() == 500) {
                    callback.setFetchingProgress(View.GONE);
                    countriesFetched = false;
                    callback.setButtonsClickable(true);
                    callback.handleResponse(context.getString(R.string.something_went_wrong), true);
                    return;
                }


                if (response.code() == 200) {
                    if (jobsTitlesFetched) {
                        callback.setFetchingProgress(View.GONE);
                        callback.setButtonsClickable(true);
                    }
                    countriesFetched = true;
                    countriesMutableLiveData.setValue(response.body().getData());
                    return;
                }

                // for any other status code not handled
                callback.setFetchingProgress(View.GONE);
                callback.setButtonsClickable(true);
                countriesFetched = false;
            }

            @Override
            public void onFailure(Call<ApiListResponse<Country>> call, Throwable t) {
                callback.setFetchingProgress(View.GONE);
                callback.setButtonsClickable(true);
                countriesFetched = false;
                callback.onFailureHandler(t, true);
            }
        });

        return countriesMutableLiveData;
    }

    private MutableLiveData<List<JobTitle>> jobTitlesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<JobTitle>> fetchJobTitles(final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE,
                context.getString(R.string.english_key)));
        Call<ApiListResponse<JobTitle>> call = MySingleton.getInstance(context).createService().jobsTitles(map);
        call.enqueue(new Callback<ApiListResponse<JobTitle>>() {
            @Override
            public void onResponse(Call<ApiListResponse<JobTitle>> call, Response<ApiListResponse<JobTitle>> response) {
                if (response.code() == 500) {
                    if (countriesFetched) {
                        callback.setFetchingProgress(View.GONE);
                        callback.setButtonsClickable(true);
                    }
                    jobsTitlesFetched = false;
                    callback.handleResponse(context.getString(R.string.something_went_wrong), true);
                    return;
                }
                if (response.code() == 200) {
                    if (countriesFetched) {
                        callback.setFetchingProgress(View.GONE);
                        callback.setButtonsClickable(true);
                    }
                    jobsTitlesFetched = true;
                    jobTitlesMutableLiveData.setValue(response.body().getData());
                    return;
                }

                // for any other status code not handled
                callback.setFetchingProgress(View.GONE);
                callback.setButtonsClickable(true);
                jobsTitlesFetched = false;
            }

            @Override
            public void onFailure(Call<ApiListResponse<JobTitle>> call, Throwable t) {
                callback.setFetchingProgress(View.GONE);
                callback.setButtonsClickable(true);
                jobsTitlesFetched = false;
                callback.onFailureHandler(t, true);
            }
        });
        return jobTitlesMutableLiveData;
    }

    public void publish(JobCreationObj job, final ModelCallback callback) {
        File file = new File(job.getImage());

        RequestBody fileRequestBody = RequestBody.create(file, MediaType.parse("image/*"));
        MultipartBody.Part part = MultipartBody.Part.createFormData("image", file.getName(), fileRequestBody);

        RequestBody token = RequestBody.create(job.getToken(), MultipartBody.FORM);
        RequestBody jobTitle = RequestBody.create(job.getJobTitle(), MultipartBody.FORM);
        RequestBody companyName = RequestBody.create(job.getCompanyName(), MultipartBody.FORM);
        RequestBody phone = RequestBody.create(job.getPhone(), MultipartBody.FORM);
        RequestBody email = RequestBody.create(job.getEmail(), MultipartBody.FORM);
        RequestBody totalExperience = RequestBody.create(String.valueOf(job.getExperience()), MultipartBody.FORM);
        RequestBody salary = RequestBody.create(String.valueOf((int) job.getSalary()), MultipartBody.FORM);
        RequestBody country = RequestBody.create(job.getCountryId(), MultipartBody.FORM);

        Map<String, RequestBody> map = new HashMap<>();
        map.put("apiToken", token);
        map.put("job_title", jobTitle);
        map.put("companyName", companyName);
        map.put("phone", phone);
        map.put("email", email);
        map.put("total_exprience", totalExperience);
        map.put("salary", salary);
        map.put("residence_country_id", country);

        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().addJob(map, part);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);

                if (response.code() == 500) {
                    callback.handleResponse(context.getString(R.string.something_went_wrong), false);
                    return;
                }

                if (response.code() == 200) {
                    callback.handleResponse(response.body().getMessage(), false);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, false);
            }
        });


    }


    protected interface ModelCallback {
        void setFetchingProgress(int searchProgress);

        void setProgress(int searchProgress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t, boolean fetchingData);

        void handleResponse(String message, boolean fetchingData);

    }
}
