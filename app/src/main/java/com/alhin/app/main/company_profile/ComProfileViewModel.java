package com.alhin.app.main.company_profile;

import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.JobCreationObj;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;
import com.bumptech.glide.Glide;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

public class ComProfileViewModel extends AndroidViewModel implements ComProfileModel.ModelCallback {
    private ComProfileModel model;
    private ViewListener viewListener;/* communicator betn view model and view */
    private ObservableField<Integer> fetchingProgress/* spinners progress */, progress/* progress */;
    private ObservableField<Boolean> buttonsClickable/* flag to check if buttons are clickable or no */;
    private ObservableField<String> fetchingErrorMessage/* error message of spinner */;
    private ObservableField<Integer> fetchingErrorView/* flag to check the visibility of spinners error view */;
    private ObservableField<String> pickedPicture;


    public ComProfileViewModel(@NonNull Application application) {
        super(application);
        model = new ComProfileModel(application);
        fetchingProgress = new ObservableField<>(View.GONE);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        fetchingErrorView = new ObservableField<>(View.GONE);
        fetchingErrorMessage = new ObservableField<>("");
        pickedPicture = new ObservableField<>("");
    }

    /*
        call model fun. to fetch countries to use for search
        show progress dialog of two spinners & disable any button then make req
     */
    protected MutableLiveData<List<Country>> requestCountries() {
        setFetchingProgress(View.VISIBLE);
        setButtonsClickable(false);
        return model.fetchCountries(this);
    }


    /*
        call model fun. to fetch jobs titles to use for search
        show progress dialog of two spinners & disable any button then make req
     */
    protected MutableLiveData<List<JobTitle>> requestJobsTitles() {
        setFetchingProgress(View.VISIBLE);
        setButtonsClickable(false);
        return model.fetchJobTitles(this);
    }

    /*
        call model fun. to fetch countries , jobTitles
        when req is failed and user retry to fetch data
        show progress dialog, disable button then make req
     */
    public void onRetryClickListener(View view) {
        setFetchingProgress(View.VISIBLE);
        setButtonsClickable(false);
        setFetchingErrorView(View.GONE);
        model.fetchCountries(this);
        model.fetchJobTitles(this);
    }

    /*
         handle error response of (jobTitle, countries) and publish reqs.
         @Param index .. flag to know error message's req.
                   |-> true => jobTitle , countries
                   |-> false => publish
     */
    @Override
    public void onFailureHandler(Throwable t, boolean fetchingData) {
        if (fetchingData) {
            setFetchingErrorView(View.VISIBLE);
            setFetchingErrorMessage(t instanceof IOException ?
                    getApplication().getString(R.string.no_internet_connection) :
                    getApplication().getString(R.string.error_fetching_data));
        } else {
            handleResponse(t instanceof IOException ?
                    getApplication().getString(R.string.no_internet_connection) :
                    getApplication().getString(R.string.error_fetching_data), false);
        }
    }

    /*
         handle response message of (jobTitle, countries) and publish reqs.
         @Param index .. flag to know message's req.
                   |-> true => jobTitle , countries
                   |-> false => publish
     */
    @Override
    public void handleResponse(String message, boolean fetchingData) {
        if (fetchingData) {
            setFetchingErrorView(View.VISIBLE);
            setFetchingErrorMessage(message);
        } else {
            if (message.toLowerCase().trim().equals(getApplication().getString(R.string.done_translatable)))
                // job is created and will reset all input if user wanna add another job
                viewListener.clearAllUserInputs();
            viewListener.showToastMessage(message);
        }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == CompanyProfileFragment.PICK_PICTURE_REQUEST_CODE) {
            Log.e("permission", grantResults[0] + "");
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                viewListener.startPicking(Intent.createChooser(intent, getApplication().getString(R.string.profile_picture)), CompanyProfileFragment.PICK_PICTURE_REQUEST_CODE);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CompanyProfileFragment.PICK_PICTURE_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            viewListener.startCroppingFrag(data.getData());
            return;
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                String img;
                Cursor cursor = getApplication().getContentResolver().query(result.getUri(), null, null, null, null);
                if (cursor == null) {
                    img = result.getUri().getPath();
                } else {
                    cursor.moveToFirst();
                    int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    img = cursor.getString(idx);
                    cursor.close();
                }
                setPickedPicture(img);
            }
        }
    }

    /*
             call model fun. to publish the job .
             all inputs are required so check if any are empty or not
             then make the req.
     */
    public void requestPublish(String jobTitleId, EditText companyNameEditText, EditText salaryEditText, EditText experienceEditText, EditText phoneNumberEditText, EditText emailEditText, String countryId) {
        if (getPickedPicture().get().isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.choose_picture));
            return;
        }
        String companyName = companyNameEditText.getText().toString().trim();
        if (companyName.isEmpty()) {
            viewListener.showEditTextError(companyNameEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String salary = salaryEditText.getText().toString().trim();
        if (salary.isEmpty()) {
            viewListener.showEditTextError(salaryEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String experience = experienceEditText.getText().toString().trim();
        if (experience.isEmpty()) {
            viewListener.showEditTextError(experienceEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String phoneNumber = phoneNumberEditText.getText().toString().trim();
        if (phoneNumber.isEmpty()) {
            viewListener.showEditTextError(phoneNumberEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty()) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.mail_not_valid));
            return;
        }

        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        model.publish(new JobCreationObj(jobTitleId, companyName, getPickedPicture().get(), Double.parseDouble(salary), Integer.parseInt(experience), phoneNumber, email,
                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""), countryId), this);
    }

    /*
         check email validation using R.E
     */
    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    @BindingAdapter("loadPickedPicture")
    public static void setPicture(ImageView view, String uri) {
        Glide.with(view.getContext())
                .load(uri)
                .error(R.mipmap.logo)
                .into(view);
    }

    /*
        Setters & Getters -------------> start
     */
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getPickedPicture() {
        return pickedPicture;
    }

    protected void setPickedPicture(String picture) {
        pickedPicture.set(picture);
    }

    public ObservableField<String> getFetchingErrorMessage() {
        return fetchingErrorMessage;
    }

    protected void setFetchingErrorMessage(String message) {
        fetchingErrorMessage.set(message);
    }

    public ObservableField<Integer> getFetchingErrorView() {
        return fetchingErrorView;
    }

    protected void setFetchingErrorView(int errorView) {
        this.fetchingErrorView.set(errorView);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getFetchingProgress() {
        return fetchingProgress;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setFetchingProgress(int progress) {
        this.fetchingProgress.set(progress);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        buttonsClickable.set(clickable);
    }
    /* \\Setters & Getters ------------> end // */

    protected interface ViewListener {
        void showToastMessage(String message);

        void startPicking(Intent intent, int requestCode);

        void startCroppingFrag(Uri uri);

        void showEditTextError(EditText editText, String errorMessage);

        void clearAllUserInputs();
    }
}

