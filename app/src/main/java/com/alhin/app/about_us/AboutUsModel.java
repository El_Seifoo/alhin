package com.alhin.app.about_us;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.AppInfo;
import com.alhin.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUsModel {
    private Context context;

    public AboutUsModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<String> stringMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<String> aboutUs(Map<String, String> map, final ModelCallback callback) {
        Call<ApiResponse<AppInfo>> call = MySingleton.getInstance(context).createService().appInfo(map);

        call.enqueue(new Callback<ApiResponse<AppInfo>>() {
            @Override
            public void onResponse(Call<ApiResponse<AppInfo>> call, Response<ApiResponse<AppInfo>> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 500) {
                    callback.handleResponse(context.getString(R.string.something_went_wrong));
                    return;
                }

                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        stringMutableLiveData.setValue(response.body().getData().getAboutUs());
                    } else {
                        callback.handleResponse(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<AppInfo>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });
        return stringMutableLiveData;
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void onFailureHandler(Throwable t);

        void handleResponse(String message);
    }
}
