package com.alhin.app.about_us;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AboutUsViewModel extends AndroidViewModel implements AboutUsModel.ModelCallback {
    private AboutUsModel model;
    private ObservableField<Integer> progress;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;

    public AboutUsViewModel(@NonNull Application application) {
        super(application);
        model = new AboutUsModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        call model fun. to fetch about us
        show progress dialog then make req
     */
    protected MutableLiveData<String> requestTermsConditions() {
        setProgress(View.VISIBLE);
        return model.aboutUs(getRequestMap(), this);
    }

    /*
        call model fun. to fetch about us when req failed
        at the first time and user retry to make req again
        show progress dialog then make req
     */
    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.aboutUs(getRequestMap(), this);
    }

    private Map<String, String> getRequestMap() {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.english_key)));
        return map;
    }

    /*
        handle error response of About us req.
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        if (t instanceof IOException)
            setErrorMessage(getApplication().getString(R.string.no_internet_connection));
        else
            setErrorMessage(getApplication().getString(R.string.error_fetching_data));
    }

    /*
        handle response of about us req.
     */
    @Override
    public void handleResponse(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }


    // Setters & Getters --------> start
    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }
    /* \\Setters & Getters ------------> end // */

}
