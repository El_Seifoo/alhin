package com.alhin.app.about_us;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.MenuItem;

import com.alhin.app.R;
import com.alhin.app.databinding.ActivityAboutUsBinding;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class AboutUsActivity extends AppCompatActivity {
    private AboutUsViewModel viewModel;
    private ActivityAboutUsBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_about_us);
        viewModel = ViewModelProviders.of(this).get(AboutUsViewModel.class);
        dataBinding.setViewModel(viewModel);

        getSupportActionBar().setTitle(getString(R.string.about_us));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.requestTermsConditions().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String data) {
                dataBinding.aboutUsText.setText(data);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
