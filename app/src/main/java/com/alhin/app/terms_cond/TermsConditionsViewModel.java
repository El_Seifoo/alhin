package com.alhin.app.terms_cond;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TermsConditionsViewModel extends AndroidViewModel implements TermsConditionsModel.ModelCallback {
    private TermsConditionsModel model;
    private ObservableField<Integer> progress;
    private ObservableField<String> errorMessage;
    private ObservableField<Integer> errorView;

    public TermsConditionsViewModel(@NonNull Application application) {
        super(application);
        model = new TermsConditionsModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        call model fun. to fetch Terms&Cond.
        show progress dialog then make req
     */
    protected MutableLiveData<String> requestTermsConditions() {
        setProgress(View.VISIBLE);
        return model.fetchTermsConditions(getRequestMap(), this);
    }

    /*
        call model fun. to fetch Terms&Cond. when req failed
        at the first time and user retry to make req again
        show progress dialog then make req
     */
    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchTermsConditions(getRequestMap(), this);
    }

    private Map<String, String> getRequestMap() {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.english_key)));
        return map;
    }

    /*
        handle error response of Terms & conditions req.
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        if (t instanceof IOException)
            setErrorMessage(getApplication().getString(R.string.no_internet_connection));
        else
            setErrorMessage(getApplication().getString(R.string.error_fetching_data));
    }

    /*
        handle response of about us req.
     */
    @Override
    public void handleResponse(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }


    // Setters & Getters --------> start
    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }
    /* \\Setters & Getters ------------> end // */

}
