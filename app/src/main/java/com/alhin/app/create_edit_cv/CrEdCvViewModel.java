package com.alhin.app.create_edit_cv;

import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.classes.Nationality;
import com.alhin.app.classes.Religion;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;
import com.alhin.app.utils.URLs;
import com.bumptech.glide.Glide;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;

public class CrEdCvViewModel extends AndroidViewModel implements CrEdCvModel.ModelCallback {
    private CrEdCvModel model;
    private ViewListener viewListener;/* communicator betn view model and view */
    private ObservableField<Integer> progress/* progress */, jobTitleProgress, nationalityProgress, countryProgress, religionProgress;
    private ObservableField<String> jobTitleErrorMessage/* error message of jobTitle & work jobTitle spinner */,
            nationalityErrorMessage/* error message of nationality spinner */,
            countryErrorMessage/* error message of countries spinner */,
            religionErrorMessage/* error message of religion spinner */;
    private ObservableField<Integer> jobTitleErrorView/* flag to check the visibility of jobTitle & work jobTitle spinner  */,
            nationalityErrorView/* flag to check the visibility of nationality spinner */,
            countryErrorView/* flag to check the visibility of countries spinner */,
            religionErrorView/* flag to check the visibility of religion spinner */;
    private ObservableField<Boolean> buttonsClickable;
    private ObservableField<String> pickedPicture;

    public CrEdCvViewModel(@NonNull Application application) {
        super(application);
        model = new CrEdCvModel(application);
        progress = new ObservableField<>(View.GONE);
        jobTitleProgress = new ObservableField<>(View.GONE);
        nationalityProgress = new ObservableField<>(View.GONE);
        countryProgress = new ObservableField<>(View.GONE);
        religionProgress = new ObservableField<>(View.GONE);

        jobTitleErrorView = new ObservableField<>(View.GONE);
        nationalityErrorView = new ObservableField<>(View.GONE);
        countryErrorView = new ObservableField<>(View.GONE);
        religionErrorView = new ObservableField<>(View.GONE);

        jobTitleErrorMessage = new ObservableField<>("");
        nationalityErrorMessage = new ObservableField<>("");
        countryErrorMessage = new ObservableField<>("");
        religionErrorMessage = new ObservableField<>("");

        buttonsClickable = new ObservableField<>(true);

        pickedPicture = new ObservableField<>("");
    }


    public void checkIsEditing(Intent intent) {
        if (intent.hasExtra("CV")) {
            CvInfo cvInfo = (CvInfo) intent.getSerializableExtra("CV");
            setPickedPicture(URLs.BASE + cvInfo.getImage());
            viewListener.setCvData(cvInfo);
        } else {
//            CvInfo info = new CvInfo();
            viewListener.setCvData(null);
        }
    }


    protected void requestSave(String jobTitle, EditText phoneEditText, EditText expectedSalaryEditText, EditText dateOfBirthEditText,
                               String nationalityId, String martialStatus, String countryId, String religionId,
                               EditText experienceEditText, String workJobTitle, EditText companyNameEditText, EditText workExperienceEditText, EditText noteEditText, boolean isEditing) {

        if (!isEditing && getPickedPicture().get().isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.choose_picture));
            return;
        }

        if (jobTitle.equals("-1")) {
            viewListener.showToastMessage(getApplication().getString(R.string.job_title_is_required));
            return;
        }

        String phone = phoneEditText.getText().toString().trim();
        if (phone.isEmpty()) {
            viewListener.showEditTextError(phoneEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }
        String expectedSalary = expectedSalaryEditText.getText().toString().trim();
        if (expectedSalary.isEmpty())
            expectedSalary = "0";

        String dateOfBirth = dateOfBirthEditText.getText().toString().trim();
        if (dateOfBirth.isEmpty()) {
            viewListener.showEditTextError(dateOfBirthEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (nationalityId.equals("-1")) {
            viewListener.showToastMessage(getApplication().getString(R.string.nationality_is_required));
            return;
        }

        if (countryId.equals("-1")) {
            viewListener.showToastMessage(getApplication().getString(R.string.residence_country_is_required));
            return;
        }

        if (religionId.equals("-1")) {
            viewListener.showToastMessage(getApplication().getString(R.string.religion_is_required));
            return;
        }


        String totalExperience = experienceEditText.getText().toString().trim();
        if (totalExperience.isEmpty()) {
            viewListener.showEditTextError(experienceEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String companyName = companyNameEditText.getText().toString().trim();
        if (companyName.isEmpty()) {
            viewListener.showEditTextError(companyNameEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        String workExperience = workExperienceEditText.getText().toString().trim();
        if (workExperience.isEmpty()) {
            viewListener.showEditTextError(workExperienceEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (workJobTitle.equals("-1")) {
            viewListener.showToastMessage(getApplication().getString(R.string.work_experience_job_title_is_required));
            return;
        }

//        if (!workJobTitle.equals("-1") && !workJobTitle.equals(getApplication().getString(R.string.job_title))) {
//            if (companyName.isEmpty()) {
//                viewListener.showEditTextError(companyNameEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
//                return;
//            }
//            if (workExperience.isEmpty()) {
//                viewListener.showEditTextError(workExperienceEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
//                return;
//            }
//        }
//
//        if (!companyName.isEmpty()) {
//            if (workJobTitle.equals("-1")) {
//                viewListener.showToastMessage(getApplication().getString(R.string.work_experience_job_title_is_required));
//                return;
//            }
//            if (workJobTitle.equals(getApplication().getString(R.string.job_title))) {
//                viewListener.showToastMessage(getApplication().getString(R.string.work_experience_job_title_is_required));
//                return;
//            }
//            if (workExperience.isEmpty()) {
//                viewListener.showEditTextError(workExperienceEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
//                return;
//            }
//        }
//
//        if (!workExperience.isEmpty()) {
//            if (workJobTitle.equals("-1")) {
//                viewListener.showToastMessage(getApplication().getString(R.string.work_experience_job_title_is_required));
//                return;
//            }
//            if (workJobTitle.equals(getApplication().getString(R.string.job_title))) {
//                viewListener.showToastMessage(getApplication().getString(R.string.work_experience_job_title_is_required));
//                return;
//            }
//            if (companyName.isEmpty()) {
//                viewListener.showEditTextError(workExperienceEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
//                return;
//            }
//        }


        setProgress(View.VISIBLE, 4);
        setButtonsClickable(false);

        if (isEditing && getPickedPicture().get().contains(URLs.BASE)) {/* update cv without photo */
            Map<String, Object> map = new HashMap<>();
            map.put("apiToken", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
            map.put("phone", phone);
            map.put("date_of_birth", dateOfBirth);
            map.put("martial_status", martialStatus);
            map.put("expectedSalary", expectedSalary);
            map.put("residence_country_id", countryId);
            map.put("religion_id", religionId);
            map.put("total_experience", totalExperience);
            map.put("job_title", jobTitle);
            map.put("nationality_id", nationalityId);
            map.put("note", noteEditText.getText().toString().trim());
            map.put("work_experience_job_title", workJobTitle);
            map.put("work_experience_company_name", companyName);
            map.put("work_experience_experirnce_years", workExperience);
            model.updateCvWithoutPhoto(map, this);
        } else if (isEditing && !getPickedPicture().get().contains(URLs.BASE)) {/* update cv with photo */
            model.updateWithPhoto(getMap(phone, dateOfBirth, martialStatus, expectedSalary, countryId, religionId,
                    totalExperience, jobTitle, nationalityId, workJobTitle, companyName, workExperience, noteEditText.getText().toString().trim()),
                    getMultiPartBodyPart(), this);
        } else if (!isEditing) {
            model.createCv(getMap(phone, dateOfBirth, martialStatus, expectedSalary, countryId, religionId,
                    totalExperience, jobTitle, nationalityId, workJobTitle, companyName, workExperience, noteEditText.getText().toString().trim()),
                    getMultiPartBodyPart(), this);
        }

    }

    private MultipartBody.Part getMultiPartBodyPart() {
        File file = new File(getPickedPicture().get());

        RequestBody fileRequestBody = RequestBody.create(MediaType.parse("image/*"), file);
        return MultipartBody.Part.createFormData("photo", file.getName(), fileRequestBody);
    }

    private Map<String, RequestBody> getMap(String phone, String dateOfBirth, String martialStatus, String expectedSalary,
                                            String countryId, String religionId, String totalExperience, String jobTitle,
                                            String nationalityId, String workJobTitle, String companyName, String workExperience, String note) {
        Map<String, RequestBody> map = new HashMap<>();
        map.put("apiToken", RequestBody.create(MultipartBody.FORM, MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, "")));
        map.put("phone", RequestBody.create(MultipartBody.FORM, phone));
        map.put("date_of_birth", RequestBody.create(MultipartBody.FORM, dateOfBirth));
        map.put("martial_status", RequestBody.create(MultipartBody.FORM, martialStatus));
        map.put("expectedSalary", RequestBody.create(MultipartBody.FORM, expectedSalary));
        map.put("residence_country_id", RequestBody.create(MultipartBody.FORM, countryId));
        map.put("religion_id", RequestBody.create(MultipartBody.FORM, religionId));
        map.put("total_experience", RequestBody.create(MultipartBody.FORM, totalExperience));
        map.put("job_title", RequestBody.create(MultipartBody.FORM, jobTitle));
        map.put("nationality_id", RequestBody.create(MultipartBody.FORM, nationalityId));
        map.put("note", RequestBody.create(MultipartBody.FORM, note));
        map.put("work_experience_job_title", RequestBody.create(MultipartBody.FORM, workJobTitle));
        map.put("work_experience_company_name", RequestBody.create(MultipartBody.FORM, companyName));
        map.put("work_experience_experirnce_years", RequestBody.create(MultipartBody.FORM, workExperience));

        return map;
    }

    /*
        call model fun. to fetch jobs titles
        show progress dialog of two spinners & disable any button then make req
     */
    protected MutableLiveData<List<JobTitle>> requestJobsTitles() {
        setProgress(View.VISIBLE, 0);
        setButtonsClickable(false);
        return model.fetchJobTitles(this);
    }

    /*
        call model fun. to fetch nationalities
        show progress dialog of two spinners & disable any button then make req
     */
    protected MutableLiveData<List<Nationality>> requestNationalities() {
        setProgress(View.VISIBLE, 1);
        setButtonsClickable(false);
        return model.fetchNationalities(this);
    }

    /*
        call model fun. to fetch countries
        show progress dialog of two spinners & disable any button then make req
     */
    protected MutableLiveData<List<Country>> requestCountries() {
        setProgress(View.VISIBLE, 2);
        setButtonsClickable(false);
        return model.fetchCountries(this);
    }

    /*
        call model fun. to fetch religions
        show progress dialog of two spinners & disable any button then make req
     */
    protected MutableLiveData<List<Religion>> requestReligions() {
        setProgress(View.VISIBLE, 3);
        setButtonsClickable(false);
        return model.fetchReligions(this);
    }

    /*
        call model fun. to fetch all entries (job title, nationality, country , religion)
        when any request failed and user tried to request them again
        @Param index ... flag to know which entry is requested
                |-> 0 => jobTitle
                |-> 1 => nationality
                |-> 2 => country
                |-> 3 => religion
     */
    public void onRetryClickListener(View view, int index) {
        setButtonsClickable(false);
        setProgress(View.VISIBLE, index);
        setErrorView(View.GONE, index);
        switch (index) {
            case 0:
                model.fetchJobTitles(this);
                break;
            case 1:
                model.fetchNationalities(this);
                break;
            case 2:
                model.fetchCountries(this);
                break;
            case 3:
                model.fetchReligions(this);
            default:
        }
    }

    /*
        handle error response of (job title, nationality, country , religion) reqs.
        @Param index ... flag to know which entry is requested
                |-> 0 => jobTitle
                |-> 1 => nationality
                |-> 2 => country
                |-> 3 => religion
                |-> 4 => updating/creating CV
     */
    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index != 4) {
            setErrorView(View.VISIBLE, index);
            setErrorMessage(t instanceof IOException ?
                    getApplication().getString(R.string.no_internet_connection) :
                    getApplication().getString(R.string.error_fetching_data), index);
        } else viewListener.showToastMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));


    }

    /*
        handle internal server response messages of (job title, nationality, country , religion) reqs.
        @Param index ... flag to know which entry is requested
                |-> 0 => jobTitle
                |-> 1 => nationality
                |-> 2 => country
                |-> 3 => religion
                |-> 4 => updating/creating CV
     */
    @Override
    public void handleResponseMessage(String message, int index) {
        if (index != 4) {
            setErrorView(View.VISIBLE, index);
            setErrorMessage(message, index);
        } else {
            if (message.trim().toLowerCase().equals(getApplication().getString(R.string.done_translatable)))
                viewListener.savedSuccessfully();
            else
                viewListener.showToastMessage(message);
        }

    }

    public void checkIsEditingJobTitle(CvInfo cv, List<JobTitle> jobTitles) {
        if (cv == null)
            viewListener.setJobTitlesList(jobTitles, null,null);
        else
            viewListener.setJobTitlesList(jobTitles, cv.getJobTitle(),cv.getWorkExpJobTitle());


    }


    public void checkIsEditingNationality(CvInfo cv, List<Nationality> nationalities) {
        if (cv == null)
            viewListener.setNationalitiesList(nationalities, null);
        else
            viewListener.setNationalitiesList(nationalities, cv.getNationality().getId());
    }

    public void checkIsEditingCountry(CvInfo cv, List<Country> countries) {
        if (cv == null)
            viewListener.setCountriesList(countries, null);
        else viewListener.setCountriesList(countries, cv.getCountry().getId());
    }

    public void checkIsEditingReligions(CvInfo cv, List<Religion> religions) {
        if (cv == null)
            viewListener.setReligionsList(religions, null);
        else viewListener.setReligionsList(religions, cv.getReligion().getId());
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("permission ReqCode", requestCode + "");
        if (requestCode == CrEdCvActivity.PICK_PICTURE_REQUEST_CODE) {
            Log.e("permission", grantResults[0] + "");
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                viewListener.startPicking(Intent.createChooser(intent, getApplication().getString(R.string.profile_picture)), CrEdCvActivity.PICK_PICTURE_REQUEST_CODE);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CrEdCvActivity.PICK_PICTURE_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            viewListener.startCroppingAct(data.getData());
            return;
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                String img;
                Cursor cursor = getApplication().getContentResolver().query(result.getUri(), null, null, null, null);
                if (cursor == null) {
                    img = result.getUri().getPath();
                } else {
                    cursor.moveToFirst();
                    int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    img = cursor.getString(idx);
                    cursor.close();
                }
                setPickedPicture(img);
            }
        }
    }


    @BindingAdapter("loadPickedPicture")
    public static void setPicture(ImageView view, String uri) {
        Glide.with(view.getContext())
                .load(uri)
                .error(R.mipmap.logo)
                .into(view);
    }

    // Setters & Getters --------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getPickedPicture() {
        return pickedPicture;
    }

    protected void setPickedPicture(String picture) {
        pickedPicture.set(picture);
    }

    public ObservableField<String> getJobTitleErrorMessage() {
        return jobTitleErrorMessage;
    }

    public ObservableField<String> getNationalityErrorMessage() {
        return nationalityErrorMessage;
    }

    public ObservableField<String> getCountryErrorMessage() {
        return countryErrorMessage;
    }

    public ObservableField<String> getReligionErrorMessage() {
        return religionErrorMessage;
    }

    public ObservableField<Integer> getJobTitleErrorView() {
        return jobTitleErrorView;
    }

    public ObservableField<Integer> getNationalityErrorView() {
        return nationalityErrorView;
    }

    public ObservableField<Integer> getCountryErrorView() {
        return countryErrorView;
    }

    public ObservableField<Integer> getReligionErrorView() {
        return religionErrorView;
    }

    public ObservableField<Integer> getJobTitleProgress() {
        return jobTitleProgress;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getNationalityProgress() {
        return nationalityProgress;
    }

    public ObservableField<Integer> getCountryProgress() {
        return countryProgress;
    }

    public ObservableField<Integer> getReligionProgress() {
        return religionProgress;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    /*
        @Param index ... flag to know which entry is requested
                    |-> 0 => jobTitle
                    |-> 1 => nationality
                    |-> 2 => country
                    |-> 3 => religion
     */
    public void setErrorMessage(String message, int index) {
        switch (index) {
            case 0:
                this.jobTitleErrorMessage.set(message);
                break;
            case 1:
                this.nationalityErrorMessage.set(message);
                break;
            case 2:
                this.countryErrorMessage.set(message);
                break;
            case 3:
            default:
                this.religionErrorMessage.set(message);
        }
    }

    /*
        @Param index ... flag to know which entry is requested
                    |-> 0 => jobTitle
                    |-> 1 => nationality
                    |-> 2 => country
                    |-> 3 => religion
     */
    public void setErrorView(int errorView, int index) {
        switch (index) {
            case 0:
                this.jobTitleErrorView.set(errorView);
                break;
            case 1:
                this.nationalityErrorView.set(errorView);
                break;
            case 2:
                this.countryErrorView.set(errorView);
                break;
            case 3:
            default:
                this.religionErrorView.set(errorView);
        }
    }

    /*
        @Param index ... flag to know which entry is requested
                    |-> 0 => jobTitle
                    |-> 1 => nationality
                    |-> 2 => country
                    |-> 3 => religion
                    |-> 4 => default progress
     */
    @Override
    public void setProgress(int progress, int index) {
        switch (index) {
            case 0:
                this.jobTitleProgress.set(progress);
                break;
            case 1:
                this.nationalityProgress.set(progress);
                break;
            case 2:
                this.countryProgress.set(progress);
                break;
            case 3:
                this.religionProgress.set(progress);
                break;
            case 4:
            default:
                this.progress.set(progress);
        }
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }
    /* \\Setters & Getters ------------> end // */

    protected interface ViewListener {
        void showToastMessage(String message);

        void setCvData(CvInfo cv);

        void setJobTitlesList(List<JobTitle> jobTitles, String jobTitle, String workExpJobTitle);

        void setWorkJobTitlesList(List<JobTitle> jobTitles, String workExpJobTitle);

        void setNationalitiesList(List<Nationality> nationalities, String id);

        void setCountriesList(List<Country> countries, String id);

        void setReligionsList(List<Religion> religions, String id);

        void startPicking(Intent intent, int requestCode);

        void startCroppingAct(Uri uri);

        void showEditTextError(EditText editText, String errorMessage);

        void savedSuccessfully();
    }
}
