package com.alhin.app.create_edit_cv;

public interface Presenter {
    void onPickPicClicked();

    void onPickDateClicked();

    void onSaveClicked();
}
