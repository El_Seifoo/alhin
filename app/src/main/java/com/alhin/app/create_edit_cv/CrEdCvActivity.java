package com.alhin.app.create_edit_cv;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.classes.Nationality;
import com.alhin.app.classes.Religion;
import com.alhin.app.databinding.ActivityCrEdCvBinding;
import com.alhin.app.main.home.JobsSpinnerAdapter;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.Calendar;
import java.util.List;

public class CrEdCvActivity extends AppCompatActivity implements CrEdCvViewModel.ViewListener, Presenter, DatePickerDialog.OnDateSetListener {
    protected static final int PICK_PICTURE_REQUEST_CODE = 1;
    private CrEdCvViewModel viewModel;
    private ActivityCrEdCvBinding dataBinding;
    private Spinner jobTitleSpinner, workJobTitleSpinner, nationalitySpinner, martialStatusSpinner, countrySpinner, religionSpinner;
    private TSpinnerAdapter<JobTitle> jobTitleAdapter, workJobTitleAdapter;
    private TSpinnerAdapter<Nationality> nationalityAdapter;
    private TSpinnerAdapter<Country> countryAdapter;
    private TSpinnerAdapter<Religion> religionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_cr_ed_cv);
        viewModel = ViewModelProviders.of(this).get(CrEdCvViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        viewModel.checkIsEditing(getIntent());

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        jobTitleSpinner = dataBinding.jobTitle;
        workJobTitleSpinner = dataBinding.workJobTitle;
        nationalitySpinner = dataBinding.nationality;
        martialStatusSpinner = dataBinding.martialStatus;
        countrySpinner = dataBinding.country;
        religionSpinner = dataBinding.religion;

        viewModel.requestJobsTitles().observe(this, new Observer<List<JobTitle>>() {
            @Override
            public void onChanged(List<JobTitle> jobTitles) {
                viewModel.checkIsEditingJobTitle(dataBinding.getCv(), jobTitles);
            }
        });

        viewModel.requestNationalities().observe(this, new Observer<List<Nationality>>() {
            @Override
            public void onChanged(List<Nationality> nationalities) {
                viewModel.checkIsEditingNationality(dataBinding.getCv(), nationalities);
            }
        });

        viewModel.requestCountries().observe(this, new Observer<List<Country>>() {
            @Override
            public void onChanged(List<Country> countries) {
                viewModel.checkIsEditingCountry(dataBinding.getCv(), countries);
            }
        });

        viewModel.requestReligions().observe(this, new Observer<List<Religion>>() {
            @Override
            public void onChanged(List<Religion> religions) {
                viewModel.checkIsEditingReligions(dataBinding.getCv(), religions);
            }
        });

    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setCvData(CvInfo cv) {
        dataBinding.setCv(cv);
    }

    @Override
    public void setJobTitlesList(List<JobTitle> jobTitles, String jobTitle, String workExpJobTitle) {
        jobTitleAdapter = new TSpinnerAdapter<>(jobTitles, CrEdCvActivity.this);
        jobTitleSpinner.setAdapter(jobTitleAdapter);
        jobTitleSpinner.setSelection(jobTitleAdapter.findPositionByName(jobTitle));

        workJobTitleSpinner.setAdapter(jobTitleAdapter);
        workJobTitleSpinner.setSelection(jobTitleAdapter.findPositionByName(workExpJobTitle));
    }

    @Override
    public void setWorkJobTitlesList(List<JobTitle> jobTitles, String workExpJobTitle) {
        workJobTitleAdapter = new TSpinnerAdapter<>(jobTitles, CrEdCvActivity.this);
        workJobTitleSpinner.setAdapter(workJobTitleAdapter);
        workJobTitleSpinner.setSelection(workJobTitleAdapter.findPositionByName(workExpJobTitle));
    }

    @Override
    public void setNationalitiesList(List<Nationality> nationalities, String id) {
        nationalityAdapter = new TSpinnerAdapter<>(nationalities, CrEdCvActivity.this);
        nationalitySpinner.setAdapter(nationalityAdapter);
        nationalitySpinner.setSelection(nationalityAdapter.findPositionById(id));
    }

    @Override
    public void setCountriesList(List<Country> countries, String id) {
        countryAdapter = new TSpinnerAdapter<>(countries, CrEdCvActivity.this);
        countrySpinner.setAdapter(countryAdapter);
        countrySpinner.setSelection(countryAdapter.findPositionById(id));
    }

    @Override
    public void setReligionsList(List<Religion> religions, String id) {
        religionAdapter = new TSpinnerAdapter<>(religions, CrEdCvActivity.this);
        religionSpinner.setAdapter(religionAdapter);
        religionSpinner.setSelection(religionAdapter.findPositionById(id));
    }

    @Override
    public void startPicking(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void startCroppingAct(Uri uri) {
        CropImage.activity(uri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(14, 10)
                .start(this);
    }

    @Override
    public void showEditTextError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
    }

    @Override
    public void savedSuccessfully() {
        showToastMessage(getString(R.string.your_cv_saved_successfully));
        setResult(RESULT_OK, getIntent());
        finish();
    }

    @Override
    public void onPickPicClicked() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PICK_PICTURE_REQUEST_CODE
        );
    }

    @Override
    public void onPickDateClicked() {
        showDatePickerDialog();
    }

    private void showDatePickerDialog() {
        DatePickerDialog datePickerDialog;
        if (dataBinding.dateOfBirth.getText().toString().trim().isEmpty())
            datePickerDialog = new DatePickerDialog(
                    this,
                    this,
                    Calendar.getInstance().get(Calendar.YEAR),
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        else {
            String date = dataBinding.dateOfBirth.getText().toString().trim();
            datePickerDialog = new DatePickerDialog(
                    this,
                    this,
                    Integer.parseInt(date.split("-")[0]),
                    Integer.parseInt(date.split("-")[1]) - 1,
                    Integer.parseInt(date.split("-")[2]));
        }

        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String dayString = String.valueOf(dayOfMonth);
        if (dayOfMonth < 10)
            dayString = "0" + dayString;
        String monthString = String.valueOf(month + 1);
        if ((month + 1) < 10)
            monthString = "0" + monthString;

        dataBinding.dateOfBirth.setText(year + "-" + monthString + "-" + dayString);
    }

    @Override
    public void onSaveClicked() {
        viewModel.requestSave(jobTitleAdapter != null ? jobTitleAdapter.findJobTitleByIndex(jobTitleSpinner.getSelectedItemPosition()) : "-1",
                dataBinding.phone, dataBinding.expectedSalary, dataBinding.dateOfBirth,
                nationalityAdapter != null ? nationalityAdapter.findIdByPosition(nationalitySpinner.getSelectedItemPosition()) : "-1", (String) martialStatusSpinner.getSelectedItem(),
                countryAdapter != null ? countryAdapter.findIdByPosition(countrySpinner.getSelectedItemPosition()) : "-1",
                religionAdapter != null ? religionAdapter.findIdByPosition(religionSpinner.getSelectedItemPosition()) : "-1",
                dataBinding.experience,
                jobTitleAdapter != null ? jobTitleAdapter.findJobTitleByIndex(workJobTitleSpinner.getSelectedItemPosition()) : "-1",
                dataBinding.companyName, dataBinding.workExperience, dataBinding.note, getIntent().hasExtra("CV"));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        viewModel.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cr_ed_cv_option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        if (item.getItemId() == R.id.save && viewModel.getButtonsClickable().get()) {
            onSaveClicked();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

