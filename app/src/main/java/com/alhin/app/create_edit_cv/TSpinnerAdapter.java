package com.alhin.app.create_edit_cv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alhin.app.R;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.classes.Nationality;
import com.alhin.app.classes.Religion;

import java.util.ArrayList;
import java.util.List;

public class TSpinnerAdapter<T> extends BaseAdapter {
    private List<T> entries;
    private Context context;
    private LayoutInflater inflater;

    public TSpinnerAdapter(List<T> entries, Context context) {
        this.entries = entries;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int findPositionByName(String name) {
        if (name == null) return 0;
        for (int i = 0; i < entries.size(); i++) {
            if (((ArrayList<JobTitle>) entries).get(i).getName().equals(name))
                return i;
        }
        return 0;
    }

    public int findPositionById(String id) {
        if (id == null) return 0;
        for (int i = 0; i < entries.size(); i++) {
            if (entries.get(i) instanceof Nationality) {
                if (((ArrayList<Nationality>) entries).get(i).getId().equals(id))
                    return i;
            } else if (entries.get(i) instanceof Country) {
                if (((ArrayList<Country>) entries).get(i).getId().equals(id))
                    return i;
            } else {
                if (((ArrayList<Religion>) entries).get(i).getId().equals(id))
                    return i;
            }
        }
        return 0;
    }

    public String findIdByPosition(int selectedItemPosition) {
        if (entries.get(selectedItemPosition) instanceof Nationality)
            return ((ArrayList<Nationality>) entries).get(selectedItemPosition).getId();
        else if (entries.get(selectedItemPosition) instanceof Country)
            return ((ArrayList<Country>) entries).get(selectedItemPosition).getId();
        else
            return ((ArrayList<Religion>) entries).get(selectedItemPosition).getId();
    }

    public String findJobTitleByIndex(int selectedItemPosition) {
        return ((ArrayList<JobTitle>) entries).get(selectedItemPosition).getName();
    }


    @Override
    public int getCount() {
        return entries.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item, null);
        }

        TextView textView = view.findViewById(R.id.spinner_text);

        if (entries.get(position) instanceof JobTitle) {
            textView.setText(((ArrayList<JobTitle>) entries).get(position).getName());
        } else if (entries.get(position) instanceof Nationality) {
            textView.setText(((ArrayList<Nationality>) entries).get(position).getNationality());
        } else if (entries.get(position) instanceof Country) {
            textView.setText(((ArrayList<Country>) entries).get(position).getName());
        } else if (entries.get(position) instanceof Religion) {
            textView.setText(((ArrayList<Religion>) entries).get(position).getReligion());
        }

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item, null);
        }

        TextView textView = view.findViewById(R.id.spinner_text);
        if (entries.get(position) instanceof JobTitle) {
            textView.setText(((ArrayList<JobTitle>) entries).get(position).getName());
        } else if (entries.get(position) instanceof Nationality) {
            textView.setText(((ArrayList<Nationality>) entries).get(position).getNationality());
        } else if (entries.get(position) instanceof Country) {
            textView.setText(((ArrayList<Country>) entries).get(position).getName());
        } else if (entries.get(position) instanceof Religion) {
            textView.setText(((ArrayList<Religion>) entries).get(position).getReligion());
        }

        return view;
    }


}
