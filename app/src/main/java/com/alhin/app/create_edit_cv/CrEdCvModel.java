package com.alhin.app.create_edit_cv;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.ApiListResponse;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.Country;
import com.alhin.app.classes.JobTitle;
import com.alhin.app.classes.Nationality;
import com.alhin.app.classes.Religion;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrEdCvModel {
    private Context context;

    public CrEdCvModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<JobTitle>> jobTitlesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<JobTitle>> fetchJobTitles(final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE,
                context.getString(R.string.english_key)));
        Call<ApiListResponse<JobTitle>> call = MySingleton.getInstance(context).createService().jobsTitles(map);
        call.enqueue(new Callback<ApiListResponse<JobTitle>>() {
            @Override
            public void onResponse(Call<ApiListResponse<JobTitle>> call, Response<ApiListResponse<JobTitle>> response) {
                callback.setProgress(View.GONE, 0);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponseMessage(context.getString(R.string.something_went_wrong), 0);
                    return;
                }
                if (response.code() == 200) {
                    if (response.body().getData() != null)
                        jobTitlesMutableLiveData.setValue(response.body().getData());
                    else callback.handleResponseMessage(response.body().getMessage(), 0);
                    return;
                }


            }

            @Override
            public void onFailure(Call<ApiListResponse<JobTitle>> call, Throwable t) {
                callback.setProgress(View.GONE, 0);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });
        return jobTitlesMutableLiveData;
    }

    private MutableLiveData<List<Nationality>> nationalitiesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Nationality>> fetchNationalities(final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE,
                context.getString(R.string.english_key)));
        Call<ApiListResponse<Nationality>> call = MySingleton.getInstance(context).createService().nationalities(map);
        call.enqueue(new Callback<ApiListResponse<Nationality>>() {
            @Override
            public void onResponse(Call<ApiListResponse<Nationality>> call, Response<ApiListResponse<Nationality>> response) {
                callback.setProgress(View.GONE, 1);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponseMessage(context.getString(R.string.something_went_wrong), 1);
                    return;
                }
                if (response.code() == 200) {
                    if (response.body().getData() != null)
                        nationalitiesMutableLiveData.setValue(response.body().getData());
                    else callback.handleResponseMessage(response.body().getMessage(), 1);
                    return;
                }


            }

            @Override
            public void onFailure(Call<ApiListResponse<Nationality>> call, Throwable t) {
                callback.setProgress(View.GONE, 1);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
        return nationalitiesMutableLiveData;
    }

    private MutableLiveData<List<Country>> countriesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Country>> fetchCountries(final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE,
                context.getString(R.string.english_key)));
        Call<ApiListResponse<Country>> call = MySingleton.getInstance(context).createService().countries(map);
        call.enqueue(new Callback<ApiListResponse<Country>>() {
            @Override
            public void onResponse(Call<ApiListResponse<Country>> call, Response<ApiListResponse<Country>> response) {
                callback.setProgress(View.GONE, 2);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponseMessage(context.getString(R.string.something_went_wrong), 2);
                    return;
                }
                if (response.code() == 200) {
                    if (response.body().getData() != null)
                        countriesMutableLiveData.setValue(response.body().getData());
                    else callback.handleResponseMessage(response.body().getMessage(), 2);
                    return;
                }


            }

            @Override
            public void onFailure(Call<ApiListResponse<Country>> call, Throwable t) {
                callback.setProgress(View.GONE, 2);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 2);
            }
        });
        return countriesMutableLiveData;
    }

    private MutableLiveData<List<Religion>> religionsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Religion>> fetchReligions(final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("language", MySingleton.getInstance(context).getStringFromSharedPref(Constants.APP_LANGUAGE,
                context.getString(R.string.english_key)));
        Call<ApiListResponse<Religion>> call = MySingleton.getInstance(context).createService().religions(map);
        call.enqueue(new Callback<ApiListResponse<Religion>>() {
            @Override
            public void onResponse(Call<ApiListResponse<Religion>> call, Response<ApiListResponse<Religion>> response) {
                callback.setProgress(View.GONE, 3);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponseMessage(context.getString(R.string.something_went_wrong), 3);
                    return;
                }
                if (response.code() == 200) {
                    if (response.body().getData() != null)
                        religionsMutableLiveData.setValue(response.body().getData());
                    else callback.handleResponseMessage(response.body().getMessage(), 3);
                    return;
                }


            }

            @Override
            public void onFailure(Call<ApiListResponse<Religion>> call, Throwable t) {
                callback.setProgress(View.GONE, 3);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 3);
            }
        });
        return religionsMutableLiveData;
    }

    protected void updateCvWithoutPhoto(Map<String, Object> map, final ModelCallback callback) {
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().updateCv(map);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                callback.setProgress(View.GONE, 4);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponseMessage(context.getString(R.string.something_went_wrong), 4);
                    return;
                }

                if (response.code() == 200) {
                    callback.handleResponseMessage(response.body().getMessage(), 4);
                    return;
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.setProgress(View.GONE, 4);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 4);
            }
        });
    }

    public void updateWithPhoto(Map<String, RequestBody> map, MultipartBody.Part multiPartBodyPart, final ModelCallback callback) {
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().updateCvWithPhoto(map, multiPartBodyPart);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                callback.setProgress(View.GONE, 4);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponseMessage(context.getString(R.string.something_went_wrong), 4);
                    return;
                }

                if (response.code() == 200) {
                    callback.handleResponseMessage(response.body().getMessage(), 4);
                    return;
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.setProgress(View.GONE, 4);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 4);
            }
        });
    }

    public void createCv(Map<String, RequestBody> map, MultipartBody.Part multiPartBodyPart, final ModelCallback callback) {
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().createCv(map, multiPartBodyPart);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                callback.setProgress(View.GONE, 4);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponseMessage(context.getString(R.string.something_went_wrong), 4);
                    return;
                }

                if (response.code() == 200) {
                    callback.handleResponseMessage(response.body().getMessage(), 4);
                    return;
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.setProgress(View.GONE, 4);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 4);
            }
        });
    }


    protected interface ModelCallback {
        void setProgress(int progress, int index);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t, int index);

        void handleResponseMessage(String message, int index);
    }
}
