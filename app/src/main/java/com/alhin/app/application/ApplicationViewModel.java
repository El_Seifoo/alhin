package com.alhin.app.application;

import android.app.Application;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.JobInfo;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class ApplicationViewModel extends AndroidViewModel implements ApplicationModel.ModelCallback {
    private ApplicationModel model;
    private ViewListener viewListener;/* communicator betn view model and view */
    private ObservableField<Integer> progress/* progress */;
    private ObservableField<Boolean> buttonsClickable/* flag to check if buttons are clickable or no */;
    private ObservableField<String> errorMessage/* error message of rec. */;
    private ObservableField<Integer> errorView/* flag to check the visibility of rec. error view */;

    public ApplicationViewModel(@NonNull Application application) {
        super(application);
        model = new ApplicationModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        call model fun. to fetch job info by its id
        show progress dialog, disable buttons then make req
     */
    private Map<String, Object> map;

    protected MutableLiveData<JobInfo> requestJobInfo(String jobId) {
        Map<String, Object> map = new HashMap<>();
        map.put("jobId", jobId);
        map.put("language", MySingleton.getInstance(getApplication())
                .getStringFromSharedPref(Constants.APP_LANGUAGE,
                        getApplication().getString(R.string.english_key)));
        this.map = map;
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        return model.fetchJobInfo(map, this);
    }

    /*
        call model fun. to fetch job info by its id
        show progress dialog, disable buttons then make req
     */
    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchJobInfo(map, this);
    }

    protected void requestApply(String jobId) {
        if (!MySingleton.getInstance(getApplication()).isLoggedIn()) {
            viewListener.loginFirst();
            return;
        }

        if (!MySingleton.getInstance(getApplication()).getBooleanFromSharedPref(Constants.IS_EMPLOYEE, true)) {
            viewListener.showToastMessage(getApplication().getString(R.string.only_employee_users_can_apply));
            return;
        }
        Map<String, String> map = new HashMap<>();
        map.put("jobId", jobId);
        map.put("apiToken", MySingleton.getInstance(getApplication())
                .getStringFromSharedPref(Constants.USER_TOKEN, ""));
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        model.apply(map, this);
    }

    /*
        handle internal server error or any message response of jobInfo req.
     */
    @Override
    public void handleResponseError(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        handle error response of jobInfo req.
        @param index .. flag to know error response's req (jobInfo or apply)
     */
    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            if (t instanceof IOException)
                setErrorMessage(getApplication().getString(R.string.no_internet_connection));
            else
                setErrorMessage(getApplication().getString(R.string.error_fetching_data));
        } else {
            viewListener.showToastMessage(t instanceof IOException ?
                    getApplication().getString(R.string.no_internet_connection) :
                    getApplication().getString(R.string.error_sending_data));
        }

    }

    /*
            handle internal server error or any message response of apply req.
     */
    @Override
    public void handleApplyResponse(String message) {
        if (message.trim().toLowerCase().equals(getApplication().getString(R.string.done_translatable))) {
            viewListener.applicationDone();
            return;
        }

        viewListener.showToastMessage(message);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ApplicationActivity.LOGIN_FIRST_REQUEST_CODE && resultCode == RESULT_OK) {
            viewListener.showToastMessage(getApplication().getString(R.string.you_can_apply_now));
        }
    }

    // Setters & Getters --------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        buttonsClickable.set(clickable);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    /* \\Setters & Getters ------------> end // */


    protected interface ViewListener {
        void showToastMessage(String message);

        void applicationDone();

        void loginFirst();
    }
}
