package com.alhin.app.application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.classes.JobInfo;
import com.alhin.app.databinding.ActivityApplicationBinding;
import com.alhin.app.login.LoginActivity;
import com.alhin.app.main.MainActivity;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class ApplicationActivity extends AppCompatActivity implements ApplicationViewModel.ViewListener, Presenter {
    protected static final int LOGIN_FIRST_REQUEST_CODE = 1;
    private ApplicationViewModel viewModel;
    private ActivityApplicationBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_application);
        viewModel = ViewModelProviders.of(this).get(ApplicationViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.requestJobInfo(getIntent().getExtras().getString("jobId")).observe(this, new Observer<JobInfo>() {
            @Override
            public void onChanged(JobInfo jobInfo) {
                dataBinding.setJob(jobInfo);
            }
        });
    }

    @Override
    public void onApplyClicked() {
        viewModel.requestApply(getIntent().getExtras().getString("jobId"));
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void applicationDone() {
        showToastMessage(getString(R.string.you_applied_successfully));
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void loginFirst() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("ApplicationActivity", "");
        startActivityForResult(loginIntent, LOGIN_FIRST_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
