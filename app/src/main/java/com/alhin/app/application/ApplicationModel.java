package com.alhin.app.application;

import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.JobInfo;
import com.alhin.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplicationModel {
    private Context context;

    public ApplicationModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<JobInfo> jobInfoMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<JobInfo> fetchJobInfo(Map<String, Object> map, final ModelCallback callback) {
        Call<ApiResponse<JobInfo>> call = MySingleton.getInstance(context).createService().jobInfo(map);
        call.enqueue(new Callback<ApiResponse<JobInfo>>() {
            @Override
            public void onResponse(Call<ApiResponse<JobInfo>> call, Response<ApiResponse<JobInfo>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponseError(context.getString(R.string.something_went_wrong));
                    return;
                }

                if (response.code() == 200) {
                    if (response.body().getMessage().toLowerCase().trim().equals(context.getString(R.string.done_translatable))) {
                        jobInfoMutableLiveData.setValue(response.body().getData());
                    } else {
                        callback.handleResponseError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<JobInfo>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 0);
            }
        });

        return jobInfoMutableLiveData;
    }

    protected void apply(Map<String, String> map, final ModelCallback callback) {
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().apply(map);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleApplyResponse(context.getString(R.string.something_went_wrong));
                    return;
                }

                if (response.code() == 200) {
                    callback.handleApplyResponse(response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t, 1);
            }
        });
    }

    protected interface ModelCallback {

        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void handleResponseError(String message);

        void onFailureHandler(Throwable t, int index);

        void handleApplyResponse(String message);
    }
}
