package com.alhin.app.signup;

public interface Presenter {
    void onSignUpButtonClicked();

    void onUserTypeClicked(boolean isEmployee);

}
