package com.alhin.app.signup;

import android.app.Application;
import android.graphics.Typeface;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.UserInfo;
import com.alhin.app.classes.UserObj;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupViewModel extends AndroidViewModel implements SignupModel.ModelCallback {
    private ViewListener viewListener;
    private SignupModel model;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonsClickable;
    private ObservableField<Boolean> employee;

    public SignupViewModel(@NonNull Application application) {
        super(application);
        model = new SignupModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
        employee = new ObservableField<>(false);
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    protected void requestRegister(EditText usernameEditText, EditText emailEditText, EditText passwordEditText, EditText confirmPasswordEditText) {
        String userName = usernameEditText.getText().toString().trim();
        if (userName.isEmpty()) {
            viewListener.showEditTextError(usernameEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }
        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty()) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.mail_not_valid));
            return;
        }

        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.password_length_error));
            return;
        }

        String confirmPassword = confirmPasswordEditText.getText().toString().trim();
        if (confirmPassword.isEmpty()) {
            viewListener.showEditTextError(confirmPasswordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (!password.equals(confirmPassword)) {
            viewListener.showEditTextError(confirmPasswordEditText, getApplication().getString(R.string.password_confirm_not_matching));
            return;
        }

        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        model.register(new UserObj(email, userName, password, MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.english_key))),
                isEmployee().get() ? getApplication().getString(R.string.employee_key) : getApplication().getString(R.string.company_key), this);
    }

    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    public ObservableField<Boolean> isEmployee() {
        return employee;
    }

    public void setEmployee(boolean employee) {
        this.employee.set(employee);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.something_went_wrong));
    }

    @Override
    public void handleResponse(ApiResponse<UserInfo> response) {
        if (response == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            return;
        }

        if (response.getData() != null)
            if (response.getMessage().toLowerCase().equals(getApplication().getString(R.string.done_translatable))) {
                MySingleton.getInstance(getApplication()).saveUserData(response.getData());
                MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.USER_TOKEN, response.getData().getToken());
                MySingleton.getInstance(getApplication()).saveBooleanToSharedPref(Constants.IS_EMPLOYEE, response.getData().isEmployee() == 1);
                MySingleton.getInstance(getApplication()).loginUser();
                viewListener.signUpDone();
                return;
            }

        viewListener.showToastMessage(response.getMessage());
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }


    protected interface ViewListener {
        void showToastMessage(String message);

        void showEditTextError(EditText editText, String error);

        void signUpDone();
    }


    @BindingAdapter("android:textStyle")
    public static void setTypeface(TextView view, String style) {
        view.setTypeface(view.getTypeface(),
                style.equals("bold") ? Typeface.BOLD : Typeface.NORMAL);
    }
}
