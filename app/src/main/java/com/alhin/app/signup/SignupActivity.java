package com.alhin.app.signup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.databinding.ActivitySignupBinding;
import com.alhin.app.main.MainActivity;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class SignupActivity extends AppCompatActivity implements SignupViewModel.ViewListener, Presenter {
    private SignupViewModel viewModel;
    private ActivitySignupBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        viewModel = ViewModelProviders.of(this).get(SignupViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle(getString(R.string.sign_up));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEditTextError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
    }

    @Override
    public void signUpDone() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onSignUpButtonClicked() {
        viewModel.requestRegister(dataBinding.usernameEditText,
                dataBinding.emailEditText,
                dataBinding.passwordEditText,
                dataBinding.confirmPasswordEditText);
    }

    @Override
    public void onUserTypeClicked(boolean isEmployee) {
        viewModel.setEmployee(isEmployee);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
