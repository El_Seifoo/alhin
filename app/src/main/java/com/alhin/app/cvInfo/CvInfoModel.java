package com.alhin.app.cvInfo;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CvInfoModel {
    private Context context;

    public CvInfoModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<CvInfo> cvInfoMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<CvInfo> fetchCvInfo(Map<String, Object> map, final ModelCallback callback) {
        Call<ApiResponse<CvInfo>> call = MySingleton.getInstance(context).createService().cvInfo(map);
        call.enqueue(new Callback<ApiResponse<CvInfo>>() {
            @Override
            public void onResponse(Call<ApiResponse<CvInfo>> call, Response<ApiResponse<CvInfo>> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 500) {
                    callback.handleResponseError(context.getString(R.string.something_went_wrong));
                    return;
                }

                if (response.code() == 200) {
                    if (response.body().getMessage().toLowerCase().trim().equals(context.getString(R.string.done_translatable))) {
                        cvInfoMutableLiveData.setValue(response.body().getData());
                    } else {
                        callback.handleResponseError(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<CvInfo>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });

        return cvInfoMutableLiveData;
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void handleResponseError(String message);

        void onFailureHandler(Throwable t);
    }
}
