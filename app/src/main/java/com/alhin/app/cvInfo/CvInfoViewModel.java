package com.alhin.app.cvInfo;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CvInfoViewModel extends AndroidViewModel implements CvInfoModel.ModelCallback {
    private CvInfoModel model;
    private ObservableField<Integer> progress/* progress */;
    private ObservableField<String> errorMessage/* error message of the view */;
    private ObservableField<Integer> errorView/* flag to check the visibility of the error view */;


    public CvInfoViewModel(@NonNull Application application) {
        super(application);
        model = new CvInfoModel(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        call model fun. to fetch cv info by its id
        show progress dialog then make req
     */
    private Map<String, Object> map;

    protected MutableLiveData<CvInfo> requestCvInfo(String cvId) {
        Map<String, Object> map = new HashMap<>();
        map.put("cvId", cvId);
        map.put("language", MySingleton.getInstance(getApplication())
                .getStringFromSharedPref(Constants.APP_LANGUAGE,
                        getApplication().getString(R.string.english_key)));
        this.map = map;
        setProgress(View.VISIBLE);
        return model.fetchCvInfo(map, this);
    }

    /*
        call model fun. to fetch cv info by its id
        show progress dialog then make req
     */
    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchCvInfo(map, this);
    }

    /*
        handle internal server error or any message response of cvInfo req.
     */
    @Override
    public void handleResponseError(String message) {
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        handle error response of cvInfo req.
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        if (t instanceof IOException)
            setErrorMessage(getApplication().getString(R.string.no_internet_connection));
        else
            setErrorMessage(getApplication().getString(R.string.error_fetching_data));
    }


    // Setters & Getters --------> start
    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }
    /* \\Setters & Getters ------------> end // */
}
