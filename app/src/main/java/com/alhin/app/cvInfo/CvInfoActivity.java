package com.alhin.app.cvInfo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.MenuItem;

import com.alhin.app.R;
import com.alhin.app.classes.CvInfo;
import com.alhin.app.databinding.ActivityCvInfoBinding;
import com.alhin.app.databinding.ActivityCvInfoBindingImpl;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class CvInfoActivity extends AppCompatActivity {
    private CvInfoViewModel viewModel;
    private ActivityCvInfoBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_cv_info);
        viewModel = ViewModelProviders.of(this).get(CvInfoViewModel.class);
        dataBinding.setViewModel(viewModel);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewModel.requestCvInfo(getIntent().getExtras().getString("cvId")).observe(this, new Observer<CvInfo>() {
            @Override
            public void onChanged(CvInfo cvInfo) {
                dataBinding.setCv(cvInfo);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
