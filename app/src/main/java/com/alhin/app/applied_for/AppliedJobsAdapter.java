package com.alhin.app.applied_for;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.alhin.app.R;
import com.alhin.app.classes.AppliedJob;
import com.alhin.app.databinding.AppliedJobsListItemBinding;

import java.util.List;

public class AppliedJobsAdapter extends RecyclerView.Adapter<AppliedJobsAdapter.Holder> {
    private List<AppliedJob> jobsList;
    private final OnItemClicked listener;

    public AppliedJobsAdapter(OnItemClicked listener) {
        this.listener = listener;
    }

    public void setJobsList(List<AppliedJob> jobsList) {
        this.jobsList = jobsList;
        notifyDataSetChanged();
    }

    protected interface OnItemClicked {
        void onJobItemClicked(View view, String appliedId, String jobId, boolean isFav);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((AppliedJobsListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.applied_jobs_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.dataBinding.setApplied(jobsList.get(position));
    }

    @Override
    public int getItemCount() {
        return jobsList != null ? jobsList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AppliedJobsListItemBinding dataBinding;

        public Holder(@NonNull AppliedJobsListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
            dataBinding.applyButton.setOnClickListener(this);
            dataBinding.favourite.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onJobItemClicked(v, dataBinding.getApplied().getId(), dataBinding.getApplied().getJob().getId(), dataBinding.getApplied().getJob().isFavourite());
        }
    }
}
