package com.alhin.app.applied_for;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.AppliedJob;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppliedJobsViewModel extends AndroidViewModel implements AppliedJobsModel.ModelCallback {
    private AppliedJobsModel model;
    private ViewListener viewListener;/* communicator betn view model and view */
    private ObservableField<Integer> progress/* list progress */;
    private ObservableField<Integer> emptyListTextView/* flag to check if (no favourits available) is visible or not */;
    private ObservableField<String> errorMessage/* error message of rec. */;
    private ObservableField<Integer> errorView/* flag to check the visibility of rec. error view */;

    public AppliedJobsViewModel(@NonNull Application application) {
        super(application);
        model = new AppliedJobsModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        call model fun. to fetch applied jobs
        show progress dialog then make req
     */
    private Map<String, String> map;

    protected MutableLiveData<List<AppliedJob>> requestAppliedJobs() {
        setProgress(View.VISIBLE);
        map = new HashMap<>();
        map.put("apiToken", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        return model.fetchAppliedJobs(map, this);
    }

    /*
        call model fun. to fetch applied jobs
        show progress dialog then make req
     */
    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchAppliedJobs(map, this);
    }

    /*
        handle error response of reqs.
        @Param index .. flag to know error message's req.
                  |-> = 0 => applied job list req.
                  |-> = 1 => un/fav & withdraw req.
     */
    @Override
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            setErrorMessage(t instanceof IOException ?
                    getApplication().getString(R.string.no_internet_connection) :
                    getApplication().getString(R.string.error_fetching_data));

        } else
            viewListener.showToastMessage(t instanceof IOException ?
                    getApplication().getString(R.string.no_internet_connection) :
                    getApplication().getString(R.string.error_fetching_data));
    }

    /*
        handle internal server error response of reqs.
        @Param index .. flag to know error message's req.
                  |-> = 0 => applied job list req.
                  |-> = 1 => un/fav & withdraw req.
     */
    @Override
    public void handleResponse(String message, int index) {
        if (index == 0) {
            setErrorView(View.VISIBLE);
            setErrorMessage(message);
        } else
            viewListener.showToastMessage(message);
    }

    /*
        check which view user clicked on view/apply button or fav icon
        if fav icon -> make favourite req.
        else -> withdraw job.
     */
    protected void handleOnJobItemClicked(View view, String appliedId, String jobId, boolean isFav) {
        switch (view.getId()) {
            case R.id.favourite:
                model.favUnFav(jobId, isFav, this);
                break;
            default:
                Map<String, String> map = new HashMap<>();
                map.put("apiToken", MySingleton.getInstance(getApplication()).getStringFromSharedPref(
                        Constants.USER_TOKEN, ""));
                map.put("appley_id", appliedId);
                model.withdraw(map, this);
        }
    }

    // Setters & Getters --------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setEmptyListTextView(int empty) {
        emptyListTextView.set(empty);
    }
    /* \\Setters & Getters ------------> end // */

    protected interface ViewListener {
        void showToastMessage(String message);
    }
}
