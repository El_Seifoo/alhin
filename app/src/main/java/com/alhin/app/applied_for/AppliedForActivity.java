package com.alhin.app.applied_for;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.classes.AppliedJob;
import com.alhin.app.databinding.ActivityAppliedForBinding;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

import java.util.List;

public class AppliedForActivity extends AppCompatActivity implements AppliedJobsViewModel.ViewListener, AppliedJobsAdapter.OnItemClicked {
    private AppliedJobsViewModel viewModel;
    private ActivityAppliedForBinding dataBinding;
    private RecyclerView list;
    private AppliedJobsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        setContentView(R.layout.activity_applied_for);
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_applied_for);
        viewModel = ViewModelProviders.of(this).get(AppliedJobsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);

        getSupportActionBar().setTitle(getString(R.string.applied_for));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        list = dataBinding.appliedJobsList;
        list.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new AppliedJobsAdapter(this);

        viewModel.requestAppliedJobs().observe(this, new Observer<List<AppliedJob>>() {
            @Override
            public void onChanged(List<AppliedJob> appliedJobs) {
                adapter.setJobsList(appliedJobs);
                list.setAdapter(adapter);
            }
        });
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onJobItemClicked(View view, String appliedId, String jobId, boolean isFav) {
        viewModel.handleOnJobItemClicked(view, appliedId, jobId, isFav);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
