package com.alhin.app.applied_for;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.ApiListResponse;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.AppliedJob;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppliedJobsModel {
    private Context context;

    public AppliedJobsModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<AppliedJob>> appliedJobsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<AppliedJob>> fetchAppliedJobs(Map<String, String> query, final ModelCallback callback) {
        Call<ApiListResponse<AppliedJob>> call = MySingleton.getInstance(context).createService().appliedJobs(query);
        call.enqueue(new Callback<ApiListResponse<AppliedJob>>() {
            @Override
            public void onResponse(Call<ApiListResponse<AppliedJob>> call, Response<ApiListResponse<AppliedJob>> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 500) {
                    callback.handleResponse(context.getString(R.string.something_went_wrong), 0);
                    return;
                }
                if (response.code() == 200) {
                    List<AppliedJob> jobs = response.body().getData();
                    if (jobs == null) {
                        callback.setEmptyListTextView(View.VISIBLE);
                        appliedJobsMutableLiveData.setValue(new ArrayList<AppliedJob>());
                    } else if (jobs.isEmpty()) {
                        callback.setEmptyListTextView(View.VISIBLE);
                        appliedJobsMutableLiveData.setValue(new ArrayList<AppliedJob>());
                    } else {
                        callback.setEmptyListTextView(View.GONE);
                        appliedJobsMutableLiveData.setValue(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiListResponse<AppliedJob>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t, 0);
            }
        });

        return appliedJobsMutableLiveData;
    }

    protected void favUnFav(final String jobId, final boolean isFav, final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("apiToken", MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        map.put("job_id", jobId);
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().favUnFav(map);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                if (response.code() == 500) {
                    callback.handleResponse(context.getString(R.string.something_went_wrong), 1);
                    return;
                }

                if (response.code() == 200) {
                    if (response.body().getMessage().equals(context.getString(R.string.done))) {
                        int index = findObjectById(appliedJobsMutableLiveData.getValue(), jobId, true);
                        if (index != -1)
                            appliedJobsMutableLiveData.getValue().get(index).getJob().setFavourite(!isFav);
                    } else callback.handleResponse(response.body().getMessage(), 1);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.onFailureHandler(t, 1);
            }
        });
    }


    protected void withdraw(final Map<String, String> map, final ModelCallback callback) {
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().withdraw(map);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
//                callback.setProgress(View.GONE);
                if (response.code() == 500) {
                    callback.handleResponse(context.getString(R.string.something_went_wrong), 1);
                    return;
                }
                if (response.code() == 200) {
                    if (response.body().getMessage().equals(context.getString(R.string.done))) {
                        int index = findObjectById(appliedJobsMutableLiveData.getValue(), map.get("appley_id"), false);
                        if (index != -1) {
                            appliedJobsMutableLiveData.getValue().remove(index);
                            if (appliedJobsMutableLiveData.getValue().isEmpty())
                                callback.setEmptyListTextView(View.VISIBLE);
                            appliedJobsMutableLiveData.setValue(appliedJobsMutableLiveData.getValue());
                        }
                    } else callback.handleResponse(response.body().getMessage(), 1);
                }

            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
//                callback.setProgress(View.GONE);
                callback.onFailureHandler(t, 1);
            }
        });
    }


    private int findObjectById(List<AppliedJob> list, String id, boolean makeFavourite) {
        for (int i = 0; i < list.size(); i++) {
            if (makeFavourite) {
                if (list.get(i).getJob().getId().equals(id))
                    return i;
            } else {
                if (list.get(i).getId().equals(id)) {
                    return i;
                }
            }

        }

        return -1;
    }

    protected interface ModelCallback {
        void setProgress(int searchProgress);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t, int index);

        void handleResponse(String message, int index);

    }
}
