package com.alhin.app.ads;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.alhin.app.classes.Ad;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdsModel {
    private Context context;

    public AdsModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<Ad> adMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<Ad> fetchAd(final ModelCallback callback) {
        Call<ApiResponse<Ad>> call = MySingleton.getInstance(context).createService().fetchAd();
        call.enqueue(new Callback<ApiResponse<Ad>>() {
            @Override
            public void onResponse(Call<ApiResponse<Ad>> call, Response<ApiResponse<Ad>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleFailedResponse();
                    return;
                }

                if (response.code() == 200) {
                    if (response.body().getData() != null)
                        adMutableLiveData.setValue(response.body().getData());
                    else
                        callback.handleFailedResponse();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Ad>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.handleFailedResponse();
            }
        });
        return adMutableLiveData;
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void handleFailedResponse();
    }
}
