package com.alhin.app.ads;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.classes.Ad;

public class AdsViewModel extends AndroidViewModel implements AdsModel.ModelCallback {
    private AdsModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonsClickable;

    public AdsViewModel(@NonNull Application application) {
        super(application);
        model = new AdsModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
    }

    /*
        call model fun. to fetch ad
        show progress dialog, disable all buttons then make req
     */
    protected MutableLiveData<Ad> requestAd() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        return model.fetchAd(this);
    }

    /*
        handle error response of About us req.
     */
    @Override
    public void handleFailedResponse() {
        viewListener.skipAd();
    }


    // Setters & Getters --------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }


    public ObservableField<Integer> getProgress() {
        return progress;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }
    /* \\Setters & Getters ------------> end // */

    protected interface ViewListener {
        void skipAd();
    }
}
