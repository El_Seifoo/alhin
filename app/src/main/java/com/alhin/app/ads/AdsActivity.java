package com.alhin.app.ads;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import com.alhin.app.R;
import com.alhin.app.classes.Ad;
import com.alhin.app.databinding.ActivityAdsBinding;
import com.alhin.app.login.LoginActivity;
import com.alhin.app.main.MainActivity;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class AdsActivity extends AppCompatActivity implements AdsViewModel.ViewListener, Presenter {
    private AdsViewModel viewModel;
    private ActivityAdsBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_ads);
        viewModel = ViewModelProviders.of(this).get(AdsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);


        viewModel.requestAd().observe(this, new Observer<Ad>() {
            @Override
            public void onChanged(Ad ad) {
                dataBinding.setAd(ad);
            }
        });
    }

    @Override
    public void skipAd() {
        onSkipClicked();
    }

    @Override
    public void onSkipClicked() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }
}
