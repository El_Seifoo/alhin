package com.alhin.app.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.alhin.app.R;
import com.alhin.app.ads.AdsActivity;
import com.alhin.app.classes.Ad;
import com.alhin.app.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {
    private static final int DELAY_TIME = 3000;
    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), AdsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        };
        handler.postDelayed(runnable, DELAY_TIME);
    }
}
