package com.alhin.app.login;

import android.app.Application;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.UserInfo;
import com.alhin.app.classes.UserObj;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginViewModel extends AndroidViewModel implements LoginModel.ModelCallback {
    private ViewListener viewListener;
    private LoginModel model;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonsClickable;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        model = new LoginModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
    }

    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    protected void requestLogin(EditText emailEditText, EditText passwordEditText) {

        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty()) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.mail_not_valid));
            return;
        }

        String password = passwordEditText.getText().toString().trim();
        if (password.isEmpty()) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (password.length() < 6) {
            viewListener.showEditTextError(passwordEditText, getApplication().getString(R.string.password_length_error));
            return;
        }

        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        model.login(new UserObj(email, password), this);
    }

    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.something_went_wrong));
    }

    @Override
    public void handleResponse(ApiResponse<UserInfo> response) {
        if (response == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            return;
        }

        String message = response.getMessage().trim().toLowerCase();

        if (message.equals(getApplication().getString(R.string.login_response_done))) {
            UserInfo userInfo = response.getData();
            MySingleton.getInstance(getApplication()).saveUserData(userInfo);
            MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.USER_TOKEN, userInfo.getToken());
            MySingleton.getInstance(getApplication()).saveBooleanToSharedPref(Constants.IS_EMPLOYEE, userInfo.isEmployee() == 1);
            MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.APP_LANGUAGE, userInfo.getLanguage());
            Language.setLanguage(getApplication(), userInfo.getLanguage());
            MySingleton.getInstance(getApplication()).loginUser();
            viewListener.loginDone();
            return;
        }

        viewListener.showToastMessage(message.equals(getApplication().getString(R.string.login_response_email_not_found)) ?
                getApplication().getString(R.string.email_not_found) :
                message.equals(getApplication().getString(R.string.login_response_wrong_password)) ?
                        getApplication().getString(R.string.wrong_password) :
                        getApplication().getString(R.string.something_went_wrong));
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    protected interface ViewListener {
        void showToastMessage(String message);

        void showEditTextError(EditText editText, String error);

        void loginDone();

    }
}
