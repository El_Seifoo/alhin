package com.alhin.app.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.databinding.ActivityLoginBinding;
import com.alhin.app.forget_password.ForgetPassActivity;
import com.alhin.app.main.MainActivity;
import com.alhin.app.signup.SignupActivity;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class LoginActivity extends AppCompatActivity implements LoginViewModel.ViewListener, Presenter {
    private LoginViewModel viewModel;
    private ActivityLoginBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.close_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (MySingleton.getInstance(this).isLoggedIn()) {
            loginDone();
        }

    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEditTextError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
    }

    @Override
    public void loginDone() {
        if (getIntent().hasExtra("ApplicationActivity")) {
            setResult(RESULT_OK, getIntent());
            finish();
            return;
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onLoginButtonClicked() {
        viewModel.requestLogin(dataBinding.emailEditText,
                dataBinding.passwordEditText);
    }

    @Override
    public void onSignUpClicked() {
        startActivity(new Intent(this, SignupActivity.class));
    }

    @Override
    public void onForgetPasswordClicked() {
        startActivity(new Intent(this, ForgetPassActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

/*
{
        company
	"email":"seif_e.attia@yahoo.com",
	"name":"seif",
	"password":"666666",
	"language":"en"
}


{       employee
	"email":"seif.e.attia@gmail.com",
	"name":"seifoo",
	"password":"666666",
	"language":"ar"
}
 */