package com.alhin.app.login;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.UserInfo;
import com.alhin.app.classes.UserObj;
import com.alhin.app.utils.MySingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginModel {
    private Context context;

    public LoginModel(Context context) {
        this.context = context;
    }

    protected void login(UserObj userObj, final ModelCallback callback) {
        /*
        code == 200 ... success
        code == 204 ... empty response
        code == 401 ... authentication
        code == 404 ... not found
        code == 500 ... internal server error
        */

        Call<ApiResponse<UserInfo>> call = MySingleton.getInstance(context).createService().login(userObj);
        call.enqueue(new Callback<ApiResponse<UserInfo>>() {
            @Override
            public void onResponse(Call<ApiResponse<UserInfo>> call, Response<ApiResponse<UserInfo>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponse(null);
                    return;
                }
                if (response.code() == 200)
                    callback.handleResponse(response.body());

            }

            @Override
            public void onFailure(Call<ApiResponse<UserInfo>> call, Throwable t) {
                Log.e("error", t.toString());
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void handleResponse(ApiResponse<UserInfo> response);
    }
}
