package com.alhin.app.login;

public interface Presenter {
    void onLoginButtonClicked();

    void onSignUpClicked();

    void onForgetPasswordClicked();
}
