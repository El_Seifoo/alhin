package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiListResponse<T> {
    private String message;
    @SerializedName(value = "residenceCountry", alternate = {"jobsName", "Jobs", "cv", "appley", "nationality", "religion"})
    private List<T> data;

    public ApiListResponse(String message, List<T> data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public List<T> getData() {
        return data;
    }
}
