package com.alhin.app.classes;

import android.widget.ImageView;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.library.baseAdapters.BR;

import com.alhin.app.R;
import com.alhin.app.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

public class JobObj extends BaseObservable {
    @SerializedName("id")
    private String id;
    @SerializedName("job_title")
    private String jobTitle;
    @SerializedName("companyName")
    private String companyName;
    @SerializedName("image")
    private String image;
    @SerializedName("rate")
    private float rate;
    @SerializedName("isFav")
    private boolean favourite;
    @SerializedName("review")
    private int reviews;
    @SerializedName("salary")
    private double salary;
    @SerializedName("total_experience")
    private int experience;


    public JobObj(String jobTitle, String companyName, String image, double salary, int experience) {
        this.jobTitle = jobTitle;
        this.companyName = companyName;
        this.image = image;
        this.salary = salary;
        this.experience = experience;
    }

    @BindingAdapter("loadImage")
    public static void loadImage(ImageView view, String image) {
        Glide.with(view.getContext())
                .load(URLs.BASE + image)
                .error(R.mipmap.logo)
                .into(view);
    }

    @BindingAdapter("loadFavIcon")
    public static void loadFav(ImageView view, boolean isFavourite) {
        view.setImageDrawable(isFavourite ? view.getResources().getDrawable(R.drawable.ic_favourite_icon) : view.getResources().getDrawable(R.drawable.ic_not_favourite_icon));
    }

    public String getId() {
        return id;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getImage() {
        return image;
    }

    public float getRate() {
        return rate;
    }

    @Bindable
    public boolean isFavourite() {
        return favourite;
    }

    public int getReviews() {
        return reviews;
    }

    public double getSalary() {
        return salary;
    }

    public int getExperience() {
        return experience;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
        notifyPropertyChanged(BR.favourite);
    }
}
