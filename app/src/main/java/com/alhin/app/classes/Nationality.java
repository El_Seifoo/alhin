package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Nationality implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("nationality")
    private String nationality;


    public String getId() {
        return id;
    }

    public String getNationality() {
        return nationality;
    }
}
