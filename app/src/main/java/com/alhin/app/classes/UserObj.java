package com.alhin.app.classes;

public class UserObj {
    private String email, name, password, language;

    public UserObj(String email, String name, String password, String language) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.language = language;
    }

    public UserObj(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getLanguage() {
        return language;
    }
}
