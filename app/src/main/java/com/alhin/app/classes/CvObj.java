package com.alhin.app.classes;

import android.widget.ImageView;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.library.baseAdapters.BR;

import com.alhin.app.R;
import com.alhin.app.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CvObj extends BaseObservable implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("job_title")
    private String jobTitle;
    @SerializedName(value = "employeeName", alternate = "Username")
    private String employeeName;
    @SerializedName("photo")
    private String image;
    @SerializedName("rate")
    private float rate;
    @SerializedName("isFav")
    private boolean favourite;
    @SerializedName("review")
    private int reviews;
    @SerializedName("expectedSalary")
    private double expectedSalary;
    @SerializedName("total_experience")
    private int experience;

    @BindingAdapter("loadImage")
    public static void loadImage(ImageView view, String image) {
        Glide.with(view.getContext())
                .load(URLs.BASE + image)
                .error(R.mipmap.logo)
                .into(view);
    }

    @BindingAdapter("loadFavIcon")
    public static void loadFav(ImageView view, boolean isFavourite) {
        view.setImageDrawable(isFavourite ? view.getResources().getDrawable(R.drawable.ic_favourite_icon) : view.getResources().getDrawable(R.drawable.ic_not_favourite_icon));
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
        notifyPropertyChanged(BR.favourite);
    }

    public String getId() {
        return id;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public String getImage() {
        return image;
    }

    public float getRate() {
        return rate;
    }

    @Bindable
    public boolean isFavourite() {
        return favourite;
    }

    public int getReviews() {
        return reviews;
    }

    public double getExpectedSalary() {
        return expectedSalary;
    }

    public int getExperience() {
        return experience;
    }
}
