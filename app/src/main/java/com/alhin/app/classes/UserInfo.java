package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

public class UserInfo extends UserObj {
    @SerializedName("apiToken")
    private String token;
    @SerializedName("logo")
    private String profPic;
    @SerializedName("is_employee")
    private int isEmployee;// 0-> company .. 1-> employee

    public UserInfo(String email, String name, String password, String language) {
        super(email, name, password, language);
    }

    public String getToken() {
        return token;
    }

    public String getProfPic() {
        return profPic;
    }

    public int isEmployee() {
        return isEmployee;
    }
}
