package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Religion implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("religion")
    private String religion;

    public String getId() {
        return id;
    }

    public String getReligion() {
        return religion;
    }
}
