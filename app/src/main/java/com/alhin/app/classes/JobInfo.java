package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

public class JobInfo extends JobObj {
    @SerializedName("phone")
    private String phone;
    @SerializedName("email")
    private String email;
    @SerializedName("residence_country")
    private Country country;
    @SerializedName("company")
    private UserInfo userInfo;


    public JobInfo(String jobTitle, String companyName, String image, double salary, int experience, String phone, String email) {
        super(jobTitle, companyName, image, salary, experience);
        this.phone = phone;
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public Country getCountry() {
        return country;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }
}
