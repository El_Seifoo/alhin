package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CvInfo extends CvObj implements Serializable {
    @SerializedName("phone")
    private String phone;
    @SerializedName("email")
    private String email;
    @SerializedName("residence_country")
    private Country country;
    @SerializedName("date_of_birth")
    private String dateOfBirth;
    @SerializedName("nationality")
    private Nationality nationality;
    @SerializedName("martial_status")
    private String martialStatus;
    @SerializedName("religion")
    private Religion religion;
    @SerializedName("note")
    private String note;
    @SerializedName("work_experience_job_title")
    private String workExpJobTitle;
    @SerializedName("work_experience_company_name")
    private String workExpCompanyName;
    @SerializedName("work_experience_experirnce_years")
    private int workExpYears;

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public Country getCountry() {
        return country;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public String getMartialStatus() {
        return martialStatus;
    }

    public Religion getReligion() {
        return religion;
    }

    public String getNote() {
        return note;
    }

    public String getWorkExpJobTitle() {
        return workExpJobTitle;
    }

    public String getWorkExpCompanyName() {
        return workExpCompanyName;
    }

    public int getWorkExpYears() {
        return workExpYears;
    }
}
