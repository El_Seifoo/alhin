package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FavouritesResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("favourite")
    private List<Favourite> favourites;

    public String getMessage() {
        return message;
    }

    public List<Favourite> getFavourites() {
        return favourites;
    }
}
