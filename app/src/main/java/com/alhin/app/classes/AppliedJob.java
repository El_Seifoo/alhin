package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

public class AppliedJob {
    @SerializedName("id")
    private String id;
    @SerializedName("job")
    private JobObj job;

    public String getId() {
        return id;
    }

    public JobObj getJob() {
        return job;
    }
}
