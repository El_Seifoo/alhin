package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

public class Favourite {
    @SerializedName("cv")
    private CvInfo cv;
    @SerializedName("job")
    private JobInfo job;

    public CvInfo getCv() {
        return cv;
    }

    public JobInfo getJob() {
        return job;
    }
}
