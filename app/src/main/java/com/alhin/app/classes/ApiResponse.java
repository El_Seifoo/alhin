package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

public class ApiResponse<T> {
    private String message;
    @SerializedName(value = "company", alternate = {"employee", "user", "jobs", "cv", "appInfo", "tmpApiToken","ads"})
    private T data;

    public ApiResponse(String message, T data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}
