package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

public class JobCreationObj extends JobInfo {
    @SerializedName("apiToken")
    private String token;
    @SerializedName("residence_country_id")
    private String countryId;

    public JobCreationObj(String jobTitle, String companyName, String image, double salary, int experience, String phone, String email, String token, String countryId) {
        super(jobTitle, companyName, image, salary, experience, phone, email);
        this.token = token;
        this.countryId = countryId;
    }

    public String getToken() {
        return token;
    }

    public String getCountryId() {
        return countryId;
    }
}
