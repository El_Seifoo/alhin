package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobTitle implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;

    public JobTitle(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
