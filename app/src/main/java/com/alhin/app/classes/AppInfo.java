package com.alhin.app.classes;

import com.google.gson.annotations.SerializedName;

public class AppInfo {
    @SerializedName("about_us")
    private String aboutUs;@SerializedName("terms_conditions")
    private String termsConditions;

    public String getAboutUs() {
        return aboutUs;
    }

    public String getTermsConditions() {
        return termsConditions;
    }
}
