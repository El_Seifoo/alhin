package com.alhin.app.classes;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.alhin.app.R;
import com.alhin.app.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

public class Ad {
    @SerializedName("id")
    private String id;
    @SerializedName("image")
    private String image;

    @BindingAdapter("loadImage")
    public static void loadImage(ImageView view, String image) {
        Glide.with(view.getContext())
                .load(URLs.BASE + image)
                .error(R.mipmap.logo)
                .into(view);
    }

    public String getId() {
        return id;
    }

    public String getImage() {
        return image;
    }
}
