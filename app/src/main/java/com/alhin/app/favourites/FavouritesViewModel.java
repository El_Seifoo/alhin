package com.alhin.app.favourites;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.application.ApplicationActivity;
import com.alhin.app.classes.Favourite;
import com.alhin.app.cvInfo.CvInfoActivity;

import java.io.IOException;
import java.util.List;

public class FavouritesViewModel extends AndroidViewModel implements FavouritesModel.ModelCallback {
    private FavouritesModel model;
    private ViewListener viewListener;/* communicator betn view model and view */
    private ObservableField<Integer> progress/* list progress */;
    private ObservableField<Integer> emptyListTextView/* flag to check if (no favourits available) is visible or not */;
    private ObservableField<String> errorMessage/* error message of rec. */;
    private ObservableField<Integer> errorView/* flag to check the visibility of rec. error view */;

    public FavouritesViewModel(@NonNull Application application) {
        super(application);
        model = new FavouritesModel(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        call model fun. to fetch favourites
        show progress dialog then make req
     */
    protected MutableLiveData<List<Favourite>> requestFavourites() {
        setProgress(View.VISIBLE);
        return model.fetchFavourites(this);
    }

    /*
        call model fun. to fetch favourites
        show progress dialog then make req
     */
    public void onRetryClickListener(View view) {
        setProgress(View.VISIBLE);
        setErrorView(View.GONE);
        model.fetchFavourites(this);
    }

    /*
        handle error response of favourites req.
     */
    @Override
    public void onFailureHandler(Throwable t) {
        setErrorView(View.VISIBLE);
        setErrorMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    /*
        handle internal server error response of favourites req.
     */
    @Override
    public void handleError500(String message, boolean makeUnFav) {
        if (makeUnFav) viewListener.showToastMessage(message);
        else {
            setErrorView(View.VISIBLE);
            setErrorMessage(message);
        }
    }

    /*
        check which view user clicked on view/apply button or fav icon
        if fav icon -> make favourite req.
        else -> navigate to cv info or job application.
     */
    public void handleOnFavouriteItemClicked(View view, String id, boolean isCV) {
        switch (view.getId()) {
            case R.id.favourite:
                model.unFav(id, isCV, this);
                break;
            default:
                if (isCV/* cv view*/)
                    viewListener.viewInfo("cvId", id, CvInfoActivity.class);
                else/* job application*/
                    viewListener.viewInfo("jobId", id, ApplicationActivity.class);

        }
    }

    // Setters & Getters --------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setEmptyListTextView(int empty) {
        emptyListTextView.set(empty);
    }

    /* \\Setters & Getters ------------> end // */


    protected interface ViewListener {
        void showToastMessage(String message);

        void viewInfo(String key, String value, Class destination);
    }
}
