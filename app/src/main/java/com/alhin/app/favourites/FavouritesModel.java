package com.alhin.app.favourites;

import android.content.Context;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.Favourite;
import com.alhin.app.classes.FavouritesResponse;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouritesModel {
    private Context context;

    public FavouritesModel(Context context) {
        this.context = context;
    }

    private MutableLiveData<List<Favourite>> favouriteMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Favourite>> fetchFavourites(final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("apiToken", MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        Call<FavouritesResponse> call = MySingleton.getInstance(context).createService().favourites(map);
        call.enqueue(new Callback<FavouritesResponse>() {
            @Override
            public void onResponse(Call<FavouritesResponse> call, Response<FavouritesResponse> response) {
                callback.setProgress(View.GONE);
                if (response.code() == 500) {
                    callback.handleError500(context.getString(R.string.something_went_wrong), false);
                    return;
                }

                if (response.code() == 200) {
                    List<Favourite> favourites = response.body().getFavourites();
                    if (favourites == null) {
                        callback.setEmptyListTextView(View.VISIBLE);
                        return;
                    }

                    if (favourites.isEmpty()) {
                        callback.setEmptyListTextView(View.VISIBLE);
                        return;
                    }
                    callback.setEmptyListTextView(View.GONE);
                    favouriteMutableLiveData.setValue(favourites);
                }
            }

            @Override
            public void onFailure(Call<FavouritesResponse> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.onFailureHandler(t);
            }
        });

        return favouriteMutableLiveData;
    }

    protected void unFav(final String id, final boolean isCv, final ModelCallback callback) {
        Map<String, String> map = new HashMap<>();
        map.put("apiToken", MySingleton.getInstance(context).getStringFromSharedPref(Constants.USER_TOKEN, ""));
        map.put(isCv ? "cv_id" : "job_id", id);
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().favUnFav(map);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                if (response.code() == 500) {
                    callback.handleError500(context.getString(R.string.something_went_wrong), true);
                    return;
                }

                if (response.code() == 200) {
                    if (response.body().getMessage().equals(context.getString(R.string.done))) {
                        int index = findObjectById(favouriteMutableLiveData.getValue(), id, isCv);
                        if (index != -1) {
                            favouriteMutableLiveData.getValue().remove(index);
                            if (favouriteMutableLiveData.getValue().isEmpty())
                                callback.setEmptyListTextView(View.VISIBLE);
                            favouriteMutableLiveData.setValue(favouriteMutableLiveData.getValue());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.onFailureHandler(t);
            }
        });
    }

    private int findObjectById(List<Favourite> favourites, String id, boolean isCv) {
        for (int i = 0; i < favourites.size(); i++) {
            if (isCv) {
                if (favourites.get(i).getCv() != null) {
                    if (favourites.get(i).getCv().getId().equals(id))
                        return i;
                }
            } else {
                if (favourites.get(i).getJob() != null) {
                    if (favourites.get(i).getJob().getId().equals(id))
                        return i;
                }
            }
        }
        return -1;
    }

    protected interface ModelCallback {
        void setProgress(int Progress);

        void setEmptyListTextView(int empty);

        void onFailureHandler(Throwable t);

        void handleError500(String message, boolean makeUnFav);
    }
}
