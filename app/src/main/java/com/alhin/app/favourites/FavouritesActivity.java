package com.alhin.app.favourites;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.classes.Favourite;
import com.alhin.app.databinding.ActivityFavouritesBinding;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

import java.util.List;

public class FavouritesActivity extends AppCompatActivity implements FavouritesViewModel.ViewListener, FavouritesAdapter.OnItemClicked {
    private ActivityFavouritesBinding dataBinding;
    private FavouritesViewModel viewModel;
    private RecyclerView favouritesList;
    private FavouritesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_favourites);
        viewModel = ViewModelProviders.of(this).get(FavouritesViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);

        getSupportActionBar().setTitle(getString(R.string.favourite));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        favouritesList = dataBinding.favouritesList;
        favouritesList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new FavouritesAdapter(this);

        viewModel.requestFavourites().observe(this, new Observer<List<Favourite>>() {
            @Override
            public void onChanged(List<Favourite> favourites) {
                adapter.setFavourites(favourites);
                favouritesList.setAdapter(adapter);
            }
        });
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void viewInfo(String key, String value, Class destination) {
        Intent intent = new Intent(this, destination);
        intent.putExtra(key, value);
        startActivity(intent);
    }

    @Override
    public void onFavouriteItemClicked(View view, String id, boolean isCV) {
        Toast.makeText(this, isCV + "", Toast.LENGTH_SHORT).show();
        viewModel.handleOnFavouriteItemClicked(view, id, isCV);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
