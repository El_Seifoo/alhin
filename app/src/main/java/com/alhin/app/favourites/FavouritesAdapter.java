package com.alhin.app.favourites;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.alhin.app.R;
import com.alhin.app.classes.Favourite;
import com.alhin.app.databinding.FavouritListItemBinding;

import java.util.List;

public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesAdapter.Holder> {
    private List<Favourite> favourites;
    private final OnItemClicked listener;

    public FavouritesAdapter(OnItemClicked listener) {
        this.listener = listener;
    }

    protected interface OnItemClicked {
        void onFavouriteItemClicked(View view, String id, boolean isCV);
    }

    public void setFavourites(List<Favourite> favourites) {
        this.favourites = favourites;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((FavouritListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.favourit_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.dataBinding.setFavourite(favourites.get(position));
    }

    @Override
    public int getItemCount() {
        return favourites != null ? favourites.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private FavouritListItemBinding dataBinding;

        public Holder(@NonNull FavouritListItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
            this.dataBinding.favourite.setOnClickListener(this);
            this.dataBinding.button.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Favourite favourite = dataBinding.getFavourite();
            listener.onFavouriteItemClicked(v, favourite.getCv() == null ? favourite.getJob().getId() : favourite.getCv().getId(), favourite.getCv() != null);
        }
    }
}
