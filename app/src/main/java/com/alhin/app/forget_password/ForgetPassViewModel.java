package com.alhin.app.forget_password;

import android.app.Application;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.classes.UserInfo;
import com.alhin.app.classes.UserObj;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.MySingleton;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForgetPassViewModel extends AndroidViewModel implements ForgetPassModel.ModelCallback {
    private ViewListener viewListener;
    private ForgetPassModel model;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonClickable;

    public ForgetPassViewModel(@NonNull Application application) {
        super(application);
        model = new ForgetPassModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonClickable = new ObservableField<>(true);
    }


    protected void requestSendCode(String email) {

        if (email.isEmpty()) {
            viewListener.showEditTextError(getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(getApplication().getString(R.string.mail_not_valid));
            return;
        }


        setProgress(View.VISIBLE);
        setButtonClickable(false);
        Map<String, String> map = new HashMap<>();
        map.put("email", email);
        model.sendCode(map, this);
    }

    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    @Override
    public void onFailureHandler(Throwable t) {
        viewListener.showToastMessage(t instanceof IOException ? getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.something_went_wrong));
    }

    @Override
    public void handleResponse(ApiResponse<Void> response) {
        if (response == null) {
            viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
            return;
        }

        String message = response.getMessage().trim();
        if (message.trim().toLowerCase().equals(getApplication().getString(R.string.done))) {
            viewListener.codeSentSuccessfully();
            return;
        }

        viewListener.showToastMessage(message.equals(getApplication().getString(R.string.login_response_email_not_found)) ?
                getApplication().getString(R.string.email_not_found) :
                getApplication().getString(R.string.something_went_wrong));
    }


    // Setters & Getters -----------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Boolean> getButtonClickable() {
        return buttonClickable;
    }

    @Override
    public void setButtonClickable(boolean clickable) {
        this.buttonClickable.set(clickable);
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }
    // Setter & getter -----------> end

    protected interface ViewListener {

        void showToastMessage(String message);

        void codeSentSuccessfully();

        void showEditTextError(String error);
    }
}
