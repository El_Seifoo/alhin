package com.alhin.app.forget_password;

public interface Presenter {
    void onGetCodeClicked();
}
