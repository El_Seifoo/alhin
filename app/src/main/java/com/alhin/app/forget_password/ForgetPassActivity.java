package com.alhin.app.forget_password;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.databinding.ActivityForgetPassBinding;
import com.alhin.app.otp.OTPActivity;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class ForgetPassActivity extends AppCompatActivity implements ForgetPassViewModel.ViewListener, Presenter {
    private ForgetPassViewModel viewModel;
    private ActivityForgetPassBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_forget_pass);
        viewModel = ViewModelProviders.of(this).get(ForgetPassViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void codeSentSuccessfully() {
        Intent intent = new Intent(this, OTPActivity.class);
        intent.putExtra("email", dataBinding.emailEditText.getText().toString().trim());
        startActivity(intent);
    }

    @Override
    public void showEditTextError(String error) {
        dataBinding.emailEditText.setError(error);
    }

    @Override
    public void onGetCodeClicked() {
        viewModel.requestSendCode(dataBinding.emailEditText.getText().toString().trim());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
