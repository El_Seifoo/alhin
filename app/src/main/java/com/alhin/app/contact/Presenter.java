package com.alhin.app.contact;

public interface Presenter {
    void onSendButtonClicked();
}
