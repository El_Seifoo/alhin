package com.alhin.app.contact;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.alhin.app.R;
import com.alhin.app.databinding.ActivityContactUsBinding;
import com.alhin.app.utils.Constants;
import com.alhin.app.utils.Language;
import com.alhin.app.utils.MySingleton;

public class ContactUsActivity extends AppCompatActivity implements ContactUsViewModel.ViewListener, Presenter {
    private ContactUsViewModel viewModel;
    private ActivityContactUsBinding dataBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.setLanguage(this, MySingleton.getInstance(this).getStringFromSharedPref(Constants.APP_LANGUAGE,
                getString(R.string.english_key)));
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_contact_us);
        viewModel = ViewModelProviders.of(this).get(ContactUsViewModel.class);
        viewModel.setViewListener(this);
        dataBinding.setViewModel(viewModel);
        dataBinding.setPresenter(this);

        getSupportActionBar().setTitle(getString(R.string.contact_us));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEditTextError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
    }

    @Override
    public void messageSent() {
        showToastMessage(getString(R.string.thanks_our_team_will_check_ur_message));
        dataBinding.emailEditText.setText("");
        dataBinding.messageEditText.setText("");
    }

    @Override
    public void onSendButtonClicked() {
        viewModel.requestSendMessage(dataBinding.emailEditText, dataBinding.messageEditText);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
