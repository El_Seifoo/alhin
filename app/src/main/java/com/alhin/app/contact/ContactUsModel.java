package com.alhin.app.contact;

import android.content.Context;
import android.view.View;

import com.alhin.app.R;
import com.alhin.app.classes.ApiResponse;
import com.alhin.app.utils.MySingleton;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsModel {
    private Context context;

    public ContactUsModel(Context context) {
        this.context = context;
    }

    protected void sendMessage(Map<String, String> map, final ModelCallback callback) {
        Call<ApiResponse<Void>> call = MySingleton.getInstance(context).createService().contactUs(map);
        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                if (response.code() == 500) {
                    callback.handleResponse(context.getString(R.string.something_went_wrong));
                    return;
                }

                if (response.code() == 200) {
                    callback.handleResponse(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                callback.setProgress(View.GONE);
                callback.setButtonsClickable(true);
                callback.onFailureHandler(t);
            }
        });
    }

    protected interface ModelCallback {
        void setProgress(int progress);

        void setButtonsClickable(boolean clickable);

        void onFailureHandler(Throwable t);

        void handleResponse(String message);
    }
}
