package com.alhin.app.contact;

import android.app.Application;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.alhin.app.R;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactUsViewModel extends AndroidViewModel implements ContactUsModel.ModelCallback {
    private ContactUsModel model;
    private ViewListener viewListener;
    private ObservableField<Integer> progress;
    private ObservableField<Boolean> buttonsClickable;

    public ContactUsViewModel(@NonNull Application application) {
        super(application);
        model = new ContactUsModel(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
    }

    /*
        call model fun. to send message
        show progress dialog, disable all buttons then make req
     */
    protected void requestSendMessage(EditText emailEditText, EditText messageEditText) {
        String email = emailEditText.getText().toString().trim();
        if (email.isEmpty()) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }
        if (!checkMailValidation(email)) {
            viewListener.showEditTextError(emailEditText, getApplication().getString(R.string.mail_not_valid));
            return;
        }

        String message = messageEditText.getText().toString().trim();
        if (message.isEmpty()) {
            viewListener.showEditTextError(messageEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
            return;
        }

        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        Map<String, String> map = new HashMap<>();
        map.put("email", email);
        map.put("message", message);
        model.sendMessage(map, this);
    }

    /*
        check email validation using R.E
     */
    private boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    /*
        handle error response of send message req.
     */
    @Override
    public void onFailureHandler(Throwable t) {
        handleResponse(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    /*
        handle response of send message req.
        by showing toast message to user
     */
    @Override
    public void handleResponse(String message) {
        if (message.equals(getApplication().getString(R.string.done))) {
            viewListener.messageSent();
            return;
        }
        viewListener.showToastMessage(message);
    }

    // Setters & Getters --------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    @Override
    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    @Override
    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }
    /* \\Setters & Getters ------------> end // */

    protected interface ViewListener {
        void showToastMessage(String message);

        void showEditTextError(EditText editText, String errorMessage);

        void messageSent();
    }
}
